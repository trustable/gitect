# How to run local tests

Support for running all tests locally is still work in progress.

## Evidence collection tests

### Testing the engine

Testing the evidence collection engine can be done via a Docker container. Run
it via the following commands:

`cd tests/engine/docker`

`docker build -f Dockerfile -t gitect-test-engine ../../..`

`docker run -e NGINX_LOGIN=test_user -e NGINX_PASSWORD=test_password -e GITECT_HOST_TOKEN=test_host_token -e GITECT_SERVICE_TOKEN=test_service_token -v $(dirname $(dirname $(dirname $PWD))):/gitect gitect-test-engine`

### Testing the REST API

Testing the evidence collection engine can be done via a Docker container. Run
it via the following commands:

`cd tests/app/docker`

`docker build -f Dockerfile -t gitect-test-app ../../..`

`docker run -e NGINX_LOGIN=test_user -e NGINX_PASSWORD=test_password -e GITECT_HOST_TOKEN=test_host_token -e GITECT_SERVICE_TOKEN=test_service_token -v $(dirname $(dirname $(dirname $PWD))):/gitect gitect-test-app`

## Artefact attestation tests

Testing the artefact attestation interface can be done via a Docker container.
Run it via the following commands:

`cd tests/client/docker`

`docker build -f Dockerfile -t gitect-test-attestation ../../..`

`docker run -v $(dirname $(dirname $(dirname $PWD))):/gitect gitect-test-attestation`
