# Configuration Guide

## GitLab Service Account

For **gitect** to communicate with the GitLab server a service account on the
GitLab server is needed.

Once the service account is created you will need to add a Personal Access Token
with api access. For instructions how to add a Personal Access Token, check the
GitLab documentation:
<https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html>.

The service account requires Developer level access to all projects that will be
instrumented with **gitect**.

## Secrets Management

Secrets management is required for server instances of **gitect**.

Create a `secrets.json` file in the `instance` directory. The file format is
defined in the schema:
`app/schema/secrets.schema.json`

Create a secret token for your instance of **gitect**. Add the login credentials
for the GitLab server service account.

## Configuration Options

A custom configuration can be defined in the `config.json` file in the `instance`
directory. The file format is defined in the schema:
`app/schema/config.json`

### EVIDENCE_TYPES

The `EVIDENCE_TYPES` option defines the types of evidence used by the
**gitect** server. Default evidence types are available, and their schemas are
defined in `schema/`. All evidence types must contain an `id` field.

### PROTOCOL

The `PROTOCOL` option defines the protocol used by the **gitect** server. The
default setting is `https`.

This option is only used by server instances of **gitect**.

### GIT_EMAIL

The `GIT_EMAIL` option defines the email address to use for pushing evidence
using git. There are cases, for instance with public Gitlab projects with the
*Committer restriction* push rule enabled, where a verified address needs to be
used for pushes to succeed. That address can be specified here. The default
setting is `gitect@example.com`.

### DATA_DIR

The `DATA_DIR` option defines the storage location for the clone repositories. The
default location is `/tmp`.

### MAX_CLONES

The `MAX_CLONES` option defines the maximum number of clones created for each repository.
The default value is 10.

### POLICY

The policy is specified for each GitLab host. A default policy for each host can
be defined and a policy can be defined on a per project basis. A project specific
policy will override any default policy for the GitLab host.

#### approval-policy

The `approval-policy` option defines the policy for approval of merge requests.

##### gitlab-ee-approvals

For GitLab Enterprise Edition hosts including gitlab.com, the
`gitlab-ee-approvals` option can be enabled to utilise the merge request approvals
functionality provided by GitLab EE.

##### Custom Approval Policy

The Custom Approval Policy functionality allows a feature to be approved by
reviewers adding comments in the merge request. It is configured using the
following parameters:

* `pattern-list` List of criteria each containing:
  * `text` Text to be matched.
  * `count` Number of comments required with the matching text.
* `unique-approvers` The number of unique approvers required.
