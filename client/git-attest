#!/bin/bash

dashless=$(basename "$0" | sed -e 's/-/ /')
export USAGE="init
   or: $dashless add -u <url> [-m | --message <message>]
   or: $dashless show [<url>] [<url>]..."

OPTIONS_SPEC=

# import git helper functions
# shellcheck source=/dev/null
. "$(git --exec-path)/git-sh-setup"

attestation_tag=ARTEFACTS_ATTEST
evidence_type=artefact-construction # XXX: change this?

# Initialise the repository for attestations management. This should only be
# needed once per repo, regardless of how many people clone it.
init () {
    tag_output=$(git tag -l "$attestation_tag")
    if [ -z "$tag_output" ]; then
        get_first_head_commit
        git tag "$attestation_tag" "$first_head_commit"
        ensure_implicit_notes_push
    else
        echo "fatal: A tag named '$attestation_tag' already exists."
    fi
}

# Read one or more attestation specified by keyword parameters to look for (one
# per attestation).
# If no keyword is present, all attestations are returned.
# If no match is found for a given keyword, no attestation is returned for it.
read_attestation () {
    ensure_notes_available
    raw_attestation=$(git notes --ref "$evidence_type" show "$attestation_tag")

    if [ "$#" -lt 1 ]; then
        result="$raw_attestation"
    else
        result=
        for ((index=1; index <= "$#"; index++)); do
            get_last_match_line "${!index}" "$raw_attestation"
            url_line="$last_match_line"

            nb_lines=$(echo "$raw_attestation" | wc -l)
            if [ "$url_line" = "$nb_lines" ]; then
                continue
            fi

            pre_url_body=$(echo "$raw_attestation" | head -n "$url_line")
            get_last_match_line "^{" "$pre_url_body"
            last_delim_line_before_url="$last_match_line"

            nb_post_url_lines=$(("$nb_lines" - "$url_line"))
            post_url_body=$(echo "$raw_attestation" \
                | tail -n "$nb_post_url_lines")
            get_first_match_line "^{" "$post_url_body"
            first_delim_line_after_url=$(("$first_match_line" + "$url_line"))

            if [ "$first_delim_line_after_url" == "$nb_lines" ]; then
                last_relevant_line="$nb_lines"
            else
                # there is a blank line between notes
                last_relevant_line=$(("$first_delim_line_after_url" - 2))
            fi

            first_att_line=$(("$last_delim_line_before_url"))
            nb_att_lines=$(("$last_relevant_line" - "$first_att_line" + 1))
            result+=$(echo "$raw_attestation" \
                | head -n "$last_relevant_line" \
                | tail -n "$nb_att_lines")
        done
    fi
}

# Write an attestation based on a URL and a message
write_attestation () {
    # parameters: url, message

    if ! git config --get user.email >/dev/null; then
        echo "Your git identity is not configured."
        echo "Please run \"git config user.email your@email.domain\""
        exit 1
    fi
    if ! git config --get user.name >/dev/null; then
        echo "Your git identity is not configured."
        echo "Please run \"git config user.name 'Your name'\""
        exit 1
    fi

    ensure_notes_available
    ensure_implicit_notes_push

    url_placeholder="~~URL~~"
    filename_placeholder="~~FILENAME~~"
    message_placeholder="~~MESSAGE~~"
    author_placeholder="~~AUTHOR~~"
    date_placeholder="~~DATE~~"
    template="{
	\"id\": \"$url_placeholder\",
	\"artefact-name\": \"$filename_placeholder\",
	\"comment\": \"$message_placeholder\",
	\"contributor-id\": \"$author_placeholder\",
	\"date-time\": \"$date_placeholder\"
}"
    filename="${1##*/}"
    username=$(git config user.name)
    email=$(git config user.email)
    date="$(date --rfc-3339=seconds)"
    json_contents=${template/$url_placeholder/$1}
    json_contents=${json_contents/$filename_placeholder/$filename}
    json_contents=${json_contents/$message_placeholder/$2}
    json_contents=${json_contents/$author_placeholder/$username <$email>}
    json_contents=${json_contents/$date_placeholder/$date}

    note_body="$json_contents"
    git notes --ref "$evidence_type" append -m "$note_body" "$attestation_tag"
    ref="refs/notes/$evidence_type"
    get_first_remote
}

# Check that notes are configured to be pushed when running `git push`, set the
# appropriate configuration directive if not
ensure_implicit_notes_push () {
    get_first_remote
    notes_ref="refs/notes/*:refs/notes/*"
    # shellcheck disable=SC2086
    if [[ "$(git config --get-all remote.$conf_first_remote.push)" != *"$notes_ref"* ]]; then
        git config --add "remote.$conf_first_remote.push" "$notes_ref"
    fi
}

# Check that notes references are available; retrieve them if not
ensure_notes_available () {
    get_first_remote
    if [[ $(git tag -l) != *"$attestation_tag"* ]]; then
        ref="refs/tags/$attestation_tag"
        git fetch "$conf_first_remote" "$ref":"$ref"
        ref="refs/notes/$evidence_type"
        git fetch "$conf_first_remote" "$ref":"$ref"
    fi
}

# Return the reference of the first (initial) commit reachable from HEAD
get_first_head_commit () {
    get_head_branch
    first_head_commit=$(git rev-list --max-parents=0 "$head_branch")
}


# Return the name of the branch that HEAD points to from the remote's point of
# view. This should return `master` in most cases.
get_head_branch () {
    root_dir=$(git rev-parse --show-toplevel)
    get_first_remote
    remotes_dir="$root_dir/.git/refs/remotes/$conf_first_remote"
    remote_no_push_errmsg="A remote must be created for $conf_first_remote, \
and data must be pushed to it."

    if [ ! -e "$remotes_dir" ]; then
        echo "fatal: $remotes_dir not found."
        echo "$remote_no_push_errmsg"
        exit 1
    fi

    head_file="$remotes_dir/HEAD"
    if [ ! -f "$head_file" ]; then
        head_file="$remotes_dir/master"
        if [ ! -f "$head_file" ]; then
            remote_heads_files=$(ls -1 "$remotes_dir")
            nb_remote_heads=$(echo "$remote_heads_files" | wc -l)
            if [ "$nb_remote_heads" -eq 0 ]; then
                 echo 'fatal: No remote head found.'
                 echo "$remote_no_push_errmsg"
                 exit 1
            fi
            head_file=$(echo "$remote_heads_files" | head -n 1)
        fi
    fi
    head_branch=$(sed 's/.*\///' "$head_file")
}

# Return the number of the first line on which a match is found for the input
# string (needle). If the string is not found, return the total number of lines
# of the context string (haystack).
# Parameters: 1. Needle
#             2. Haystack
get_first_match_line () {
    get_match_lines "$1" "$2"
    first_match_line=$(echo "$match_lines" | head -n 1)
}

# Return the number of the last line on which a match is found for the input
# string (needle). If the string is not found, return the total number of lines
# of the context string (haystack).
# Parameters: 1. Needle
#             2. Haystack
get_last_match_line () {
    get_match_lines "$1" "$2"
    last_match_line=$(echo "$match_lines" | tail -n 1)
}

# Return lines containing matches for the given string (needle), or the number
# of lines of the context string (haystack) if said string is not found.
# Parameters: 1. Needle
#             2. Haystack
get_match_lines () {
    pattern=$(echo "$1" | sed 's/\//\\\//g')
    match_lines=$(echo "$2" | awk "/$pattern/ {print FNR}")

    if [ -z "$match_lines" ]; then
        match_lines=$(echo "$2" | wc -l)
    fi
}

get_first_remote () {
    conf_first_remote="$(git remote | head -n 1)"
    if [ -z "$conf_first_remote" ]; then
        echo 'fatal: no remote found.'
        echo 'A remote needs to be configured with *git remote add*'
        exit 1
    fi
}

message=
url=
action=
while [ "$#" != 0 ]; do
    case "$1" in
    init)
            shift
            init "$@"
            ;;
    add)
            shift
            action="add"
            ;;
    show)
            shift
            read_attestation "$@"
            if [ -n "$result" ]; then
                echo -e "$result"
            fi
            shift
            ;;
    -m|--message)
            shift
            message="$1"
            shift
            ;;
    -u)
            shift
            url="$1"
            shift
            ;;
    -h)
            echo "$OPTIONS_SPEC"
            ;;
    *)
            usage
            ;;
    esac
done

if [ "$action" = "add" ]; then
    if [ -z "$url" ]; then
        usage
    fi
    if [ -z "$message" ]; then
        msg_filename=attestation_$(date +%s)
        editor=$(git config core.editor)
        if [ -z "$editor" ]; then
            editor="$GIT_EDITOR"
        fi
        "$editor" "$msg_filename"
        message=$(cat "$msg_filename")
    fi
    write_attestation "$url" "$message"
    if [ -f "$msg_filename" ]; then
        rm "$msg_filename"
    fi
fi
