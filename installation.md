# Installation Guide

**gitect** is comprised of two components, the evidence collection system and
the (optional) client interface.

## Evidence Collection System

This is developed in python (3.x) and requires Git (v2.1.4+).

The evidence collection system can be deployed as a server instance for usage of
the gitect REST API or as a python script for usage of the gitect Engine
API.

Any number of server instances or local instances may be used.

### gitect REST API Installation

A server installation of **gitect** uses the Flask library.

The server needs to be deployed where it can access to the GitLab server and can
be accessed by the GitLab server.

#### Direct Installation

To install directly on a Debian based distribution, clone the repository and
execute the following instructions from the gitect directory:

`sudo apt-get install -y python3-venv`

`python3 -m venv venv`

`source venv/bin/activate`

`pip install -r requirements.txt`

For other platforms refer to the flask installation instructions
<http://flask.pocoo.org/docs/1.0/installation>.

To start the application on localhost in development mode:

`export FLASK_APP=gitect.py`

`export FLASK_ENV=development`

`flask run`

#### Docker Installation

To run in a docker container on localhost in development mode, clone the
repository and execute the following instructions from the gitect directory:

`sudo docker build -t gitect .`

`sudo docker run -d -p 5000:5000 -e FLASK_ENV=development --name gitect gitect`

### gitect Engine API Installation

To install **gitect** to use the Engine API, install as for a direct
installation but do not start the application.

## Client Interface

The Artefact Attestation Interface is a Bash shell based command line tool that
operates in a similar manner to other Git commands. It is available via
`client/git-attest`, and should be functional after going through the following
steps:

    1. Copy `git-attest` to a directory that is in the system `$PATH`.
    2. Change the working directory to the target repository's working tree.
    3. Execute `git attest init` to initialise the repository for attestations.

Execute `git attest -h` to see a list of available options.
