# Usage Guide

## Git Server Configuration

**gitect** currently supports GitLab as the Git server.

To configure a project for usage with **gitect** the GitLab repository should be
configured as detailed in the following sections.

### Master Branch

Configure the master branch as a
[protected branch](https://docs.gitlab.com/ee/user/project/protected_branches.html).

It is recommended to configure the `Allowed to push` option to `None`. If pushes
to the master branch are allowed then no direct evidence will be generated for
any changes pushed directly to the master branch.

### Merge Method

Navigate to **Settings -> General -> Merge method** for the project, and configure
the `Merge method` option as `Merge commit with semi-linear history`.

### Merge Approvals

#### GitLab Enterprise Edition / GitLab.com

GitLab Enterprise Edition (and GitLab.com) include a
[Merge request approvals](https://gitlab.com/help/user/project/merge_requests/merge_request_approvals)
feature. To integrate this feature with **gitect** enable the
`gitlab-ee-approvals` option in the **gitect** [configuration](configuration.md).

#### GitLab Community Edition

GitLab Community Edition users can configure a custom **gitect** approvals
policy. This feature allows merge requests to be approved by using keyword
entries in Merge Request comments. For more details regarding approvals
configuration settings refer to the [Configuration Guide](configuration.md).

It is recommended to enable the
`Only allow merge requests to be merged if the pipeline succeeds` option in the
project Merge request settings to enforce the approvals policy. In addition, when
integrating the [Policy Compliance API](#policy-compliance-api) the CI pipeline
should be configured to fail if the policy compliance check fails.

## Project Instrumentation

Instrumentation of a project with **gitect** requires integration of the gitect
REST API (Evidence API, Policy Compliance API and Merges API) or the gitect
Engine API.

The gitect REST API is described in the [gitect REST API reference](gitect-api-v1.html).

### Evidence API

The CI pipeline is triggered by the submission of a change to the change tracker.
The subsequent sequence of events is illustrated in the
[change submission sequence diagram](https://gitlab.com/trustable/documents/blob/master/deprecated/change-submission-sequence-diagram.md).
With the default **gitect** configuration, Evidence can be collected at the
following stages during the CI execution for a feature branch:

* Environment Construction Evidence produced by the Orchestrator during
  construction of the build environments.

* Artefact Deployment Evidence produced by the Orchestrator during
  deployment of artefacts to the build environments.

* Artefact Construction Evidence produced by the Constructor during
  construction of artefacts.

* Environment Construction Evidence produced by the Orchestrator during
  construction of the validation environments.

* Artefact Deployment Evidence produced by the Orchestrator during
  deployment of artefacts to the validation environments.

* Change Validation Evidence produced by the Change Validator during validation
  of the changes.

The evidence can be stored by submitting a POST request to the Evidence API.

As an alternative to the gitect REST API, the gitect Engine API
`write_evidence` function can be called directly using a local instance
of **gitect**.

The evidence generation should be performed on all feature branches, i.e. those
containing changes that will be merged directly to the master branch. Evidence
should not be generated on the master branch.

### Policy Compliance API

The policy compliance check must be performed on all feature branches.

This should form the final stage in the CI pipeline. To check policy compliance
submit a GET request to the Policy Compliance API from the Gate.

The request could be submitted to the Policy Compliance API using
[curl](https://curl.haxx.se/) as in the following example:

```bash
curl -H "Accept: application/json" -H "X-Gitect-Token: <gitect-token>" \
-G --data-urlencode "validation_id=<validation-id1>,<validation-id2>" \
"https://<gitect-hostname>/api/v1/<host>/<project>/~/<feature-branch>/ \
<commit-ref>/policy-compliance"
```

Note: Even if no specific policy is defined for the project, the Policy
Compliance API must be called to ensure that the Change Review Evidence is
correctly generated.

As an alternative to the gitect REST API, the gitect Engine API
`check_policy_compliance` function can be called directly using a local instance
of **gitect**.

```bash
result=$(python -c 'import engine.policy; \
    print(engine.policy.check_policy_compliance("host", "project", \
    "host-account", "host-token", "feature-branch", "commit-ref", "master", \
    ["validation-id1", "validation-id2"]))'| tail -1)
```

### Merges API

Each project should have a webhook configured using the following procedure:

<https://docs.gitlab.com/ee/user/project/integrations/webhooks.html>

The input parameters should be configured as follows:

* `URL` URL for the **gitect** instance Merges API.

* `Secret Token` secret token created for the **gitect** instance.

* `Trigger` Merge request events enabled.

It is also possible to configure a service template in GitLab to apply to all
projects:

<https://docs.gitlab.com/ee/user/project/integrations/services_templates.html>

## Artefact Attestation

The project may use binary artefacts supplied by an external team or third party.
As these artefacts are not constructed within the project there will be no
artefact construction evidence generated. To provide a trustable chain of
custody for such artefacts the Reviewer is responsible for assessing the
trustability of the artefact and attesting to its provenance.

The Artefact Attestation Interface allows the reviewer to record the evidence of
artefact attestation in the repository.

To install the Artefact Attestation Interface refer to the
[Installation Guide](installation.md).

To use the Artefact Attestation Interface first change the working directory to
the target repository's working tree.

To add an artefact attestation, execute the following command:
`git attest add -u <URL> -m <message>`

To view the artefact attestation evidence for an artefact, execute the following
command: `git attest show <keyword>`

To view the artefact attestation evidence for all artefacts, execute the following
command: `git attest show`

Execute `git attest -h` to see a list of available options.

## Evidence Retrieval

### Evidence API: GET Request

A GET request can be submitted to the Evidence API to retrieve evidence items or
collections of evidence for a specific change commit and evidence type.
For details of the Evidence API refer to the [gitect REST API reference](gitect-api-v1.html).

Artefact attestation evidence can also be retrieved via the Evidence API. The
`branch` field should be set to the master branch, the `commit` field to
`ARTEFACTS_ATTEST` and the `evidence-type` field to `artefact-construction`.

### History API

The History API can be used to retrieve the full evidence history of the project.
For details of the History API refer to the [gitect REST API reference](gitect-api-v1.html).
