import datetime
from engine.evidence import read_evidence, write_evidence
from engine.git_handler import GitHandler
from engine.gitlab_adapter import read_merge_request, read_merge_request_approvals
from engine.errors import PolicyError, GitServerError, ValidationError
import engine.config

def check_policy_compliance(host, project, username, password, branch, commit,
                            target_branch, validation_id_list):
    """ Checks policy compliance and writes change review evidence.

    Checks whether the merge request is compliant with the policy and writes
    change review evidence.

    Args:
        host (str): FQDN of Git host
        project (str): Project path with namespace
        username (str): Service account username
        password (str): Service account access token
        branch (str): Source branch
        commit (str): SHA of revision
        target_branch (str): Target branch
        validation_id_list: List of ids change validation evidence for this commit

    Raises:
        PolicyError: If commit is not compliant with policy
        GitServerError: If Git server error
        ValidationError: If validation error

    """
    git_handler = GitHandler()
    try:
        git_handler.open(host, project, username, password, branch)
        commit_list = git_handler.read_commit_list(target_branch, commit)
        git_handler.release()
    except GitServerError:
        pass

    change_review_evidence = {}
    change_review_evidence['schema-version'] = "1"
    change_review_evidence['feature'] = branch
    change_review_evidence['change-submission-id-list'] = commit_list
    change_review_evidence['change-validation-id-list'] = validation_id_list
    change_review_evidence['date-time'] = datetime.datetime.utcnow().isoformat()

    try:
        merge_request = read_merge_request(host, project, password, branch,
                                           target_branch, False)
        change_review_evidence['id'] = '{}-{}'.format(branch, merge_request['iid'])
        change_review_evidence['comments'] = merge_request['merge_comments']

        approvers = _approvals(host, project, password, merge_request)
        change_review_evidence['reviewer-id-list'] = approvers
    except (PolicyError, GitServerError) as ex:
        change_review_evidence['id'] = '{}-{}'.format(branch, commit)
        change_review_evidence['comments'] = []
        change_review_evidence['reviewer-id-list'] = []
        raise ex
    finally:
        write_evidence(host,
                       project,
                       username,
                       password,
                       branch,
                       commit,
                       'change-review',
                       [change_review_evidence])

def merge(host, project, username, password, branch, target_branch, merge_commit_sha):
    """Merges evidence to target branch merge commit.

   Args:
        host (str): FQDN of Git host
        project (str): Project path with namespace
        username (str): Service account username
        password (str): Service account access token
        branch (str): Source branch
        target_branch (str): Target branch
        merge_commit_sha (str): SHA of merge commit

    Raises:
        PolicyError: If commit is not compliant with policy
        GitServerError: If Git server error
        ValidationError: If validation error

    """
    error = None
    merge_request = read_merge_request(host, project, password, branch,
                                       target_branch, True)

    evidence_types = engine.config.get('EVIDENCE_TYPES')
    written_evidence_type_count = 0

    for evidence_type in evidence_types:
        evidence_list = read_evidence(host, project, username, password, branch,
                                      merge_request['sha'], [evidence_type])
        if evidence_list:
            if evidence_type == 'change-review':
                # update the approvers and comments before writing the evidence
                evidence_list[-1]['id'] = '{}-{}'.format(branch, merge_request['iid'])
                evidence_list[-1]['comments'] = merge_request['merge_comments']

                try:
                    approvers = _approvals(host, project, password, merge_request)
                    evidence_list[-1]['reviewer-id-list'] = approvers
                except (PolicyError, GitServerError) as ex:
                    error = ex

            try:
                write_evidence(host,
                               project,
                               username,
                               password,
                               target_branch,
                               merge_commit_sha,
                               evidence_type,
                               evidence_list)
                written_evidence_type_count += 1
            except (ValidationError, GitServerError) as ex:
                error = ex

    if error:
        # This can be raised now, as much evidence as possible has been copied
        # onto the merge commit
        raise error #pylint: disable=raising-bad-type


def _approvals(host, project, password, merge_request):
    """Checks approvals policy.

    Args:
        host (str): FQDN of Git host
        project (str): Project path with namespace
        password (str): Service account access token
        merge_request (dict): Merge request

    Raises:
        PolicyError: If not compliant with approvals policy
        GitServerError: If Git server error

    """
    approvals_policy = {}
    approvals_policy.update(engine.config.get('POLICY', {}).get(host, {})
                            .get('default', {}).get('approval-policy', {}))
    approvals_policy.update(engine.config.get('POLICY', {}).get(host, {})
                            .get(project, {}).get('approval-policy', {}))

    if approvals_policy.get('gitlab-ee-approvals', False):
        approvers = read_merge_request_approvals(host, password,
                                                 merge_request['project_id'],
                                                 merge_request['iid'])
        return approvers

    # Non-Gitlab EE approvals policy
    approvers = set()
    for approval in approvals_policy.get('pattern-list', []):
        approval_count = approval.get('count', 1)
        for mr_comment in merge_request['merge_comments']:
            if approval.get('text', '') in mr_comment['message']:
                approvers.add(mr_comment['author']['username'])
                approval_count -= 1
                if not approval_count:
                    break
        if approval_count:
            message = '%d reviewer(s) are required to approve feature'\
                               ' changelist with approval text %s.' %\
                               (approval.get('count', 1),
                                approval.get('text', ''))
            engine.logger.info(message)
            raise PolicyError(message)

    if len(approvers) < approvals_policy.get('unique-approvers', 0):
        message = '%d unique reviewers are required to approve feature'\
                           ' changelist.' %\
                           approvals_policy.get('unique-approvers', 0)
        engine.logger.info(message)
        raise PolicyError(message)

    return list(approvers)
