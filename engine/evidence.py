import json
from engine.git_handler import GitHandler
from engine.validate import validate_evidence_list, validate_evidence_type

def read_evidence(host, project, username, password, branch, commit,
                  evidence_type_list, git_handler=None):
    """Reads evidence of the specified types from the specified commit note.

    Args:
        host (str): FQDN of Git host
        project (str): Project path with namespace
        username (str): Service account username
        password (str): Service account access token
        branch (str): Source branch
        commit (str): SHA of revision
        evidence_type_list: List of evidence types to read
        git_handler (obj): GitHandler object to use

    Returns:
        Evidence List or None if no evidence found

    Raises:
        GitServerError: If Git server error

    """
    if git_handler:
        keep_git_handler = True
    else:
        keep_git_handler = False
        git_handler = GitHandler()
        git_handler.open(host, project, username, password, branch)

    evidence_list = []
    for evidence_type in evidence_type_list:
        raw_notes = git_handler.read_note(evidence_type, commit)

        # read raw note and format output as an array of evidence elements
        if raw_notes:
            notes = raw_notes.decode().splitlines()
            for note in notes:
                try:
                    evidence_list.append(json.loads(note))
                except ValueError:
                    # ignore lines that are not valid JSON data
                    pass

    if not keep_git_handler:
        git_handler.release()

    return evidence_list


def write_evidence(host, project, username, password, branch, commit,
                   evidence_type, evidence_list, validated=False):
    """Writes the evidence of the specified type to the specified commit note.

    If `validated` is set to True, then the evidence data is considered to have
    been already validated, hence no further validation is performed.


    Args:
        host (str): FQDN of Git host
        project (str): Project path with namespace
        username (str): Service account username
        password (str): Service account access token
        branch (str): Source branch
        commit (str): SHA of revision
        evidence_type: Type of evidence to write
        evidence_list: List of evidence
        validated (bool): True is evidence already validated

    Raises:
        ValidationError: If evidence data is invalid
        GitServerError: If Git server error

    """
    if not validated:
        validate_evidence_type(evidence_type)
        validate_evidence_list(evidence_type, evidence_list)

    git_handler = GitHandler()
    git_handler.open(host, project, username, password, branch)

    # write notes
    for evidence in evidence_list:
        git_handler.write_note(evidence_type, commit, json.dumps(evidence))

    git_handler.push_notes(evidence_type)
    git_handler.release()
