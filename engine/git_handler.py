import os
import subprocess
from subprocess import call, check_output
import fcntl
import errno
import time
from random import random
from engine.errors import GitServerError
import engine.config

DEFAULT_DIR = '/tmp'
LOCK_RETRIES = 10
DEFAULT_MAX_CLONES = 10

class GitHandler():
    """Handler for Git interface."""

    def __init__(self):
        self.git_dir_lock = None
        self.git_dir = None
        self.clone_index = 0

    def open(self, host, project, username, password, branch):
        """ Opens a clone of the repo for execution of Git commands.

        Iterates each existing clone and tries to place a lock. If unable to lock
        any clone after retries then creates a new clone. Once the clone is ready,
        performs a checkout on the specified branch.

        Args:
            host (str): FQDN of Git host
            project (str): Project path with namespace
            username (str): Service account username
            password (str): Service account access token
            branch (str): Source branch

        Raises:
            GitServerError: If Git command error

        """
        retries = LOCK_RETRIES
        while True:
            self.git_dir = '{}/{}/{}_{}.git'.format(engine.config.get('DATA_DIR', DEFAULT_DIR),
                                                    host,
                                                    project,
                                                    self.clone_index)
            if os.path.exists(self.git_dir):
                if self._lock():
                    # locked existing clone
                    break
            else:
                if retries and self.clone_index:
                    # random delay then try the existing clone pool again
                    retries -= 1
                    self.clone_index = 0
                    time.sleep(random())
                    continue

                # create new clone
                # ensure path to git_dir exists
                os.makedirs(self.git_dir, exist_ok=True)

                if not self._lock():
                    continue

                self._create_new_clone(host, project, username, password)
                break

            self.clone_index += 1
            if self.clone_index >= engine.config.get('MAX_CLONES', DEFAULT_MAX_CLONES):
                message = 'Max number of clones already in use'
                engine.logger.info(message)
                raise GitServerError(message)

        self._checkout_branch(branch)


    def _lock(self):
        """Tries to lock the clone and returns the result.

        Returns:
            True if able to lock clone, False if already locked

        Raises:
            IOError: If filesystem IO error occurred

        """
        self.git_dir_lock = open('{}.lock'.format(self.git_dir), 'w+')
        try:
            fcntl.flock(self.git_dir_lock, fcntl.LOCK_EX | fcntl.LOCK_NB)
            return True
        except IOError as ex:
            if ex.errno != errno.EAGAIN:
                raise
            engine.logger.info("Clone locked")
            self.git_dir_lock.close()
            return False


    def release(self):
        """Releases the lock on the clone."""
        fcntl.flock(self.git_dir_lock, fcntl.LOCK_UN)
        self.git_dir_lock.close()


    def _create_new_clone(self, host, project, username, password):
        """Creates a new clone of the project.

        Args:
            host (str): FQDN of Git host
            project (str): Project path with namespace
            username (str): Service account username
            password (str): Service account access token
            branch (str): Source branch

        Raises:
            GitServerError: If Git command error

        """
        try:
            if call(['git', 'clone',
                     '{}://{}:{}@{}/{}.git'.format(engine.config.get('PROTOCOL', 'https'),
                                                   username, password, host, project),
                     self.git_dir]) != 0:
                self.release()
                message = 'Git clone failed'
                engine.logger.info(message)
                raise GitServerError(message)

            call(['git', 'config', 'user.name', 'gitect'], cwd=self.git_dir)
            email = engine.config.get('GIT_EMAIL', 'gitect@example.com')
            call(['git', 'config', 'user.email', email], cwd=self.git_dir)
            # add all notes to refs
            if call(['git', 'config', '--add', 'remote.origin.fetch',
                     '+refs/notes/*:refs/notes/*'], cwd=self.git_dir) != 0:
                self.release()
                message = 'Git error setting config'
                engine.logger.info(message)
                raise GitServerError(message)

        except subprocess.CalledProcessError:
            self.release()
            message = 'Git command exception creating clone'
            engine.logger.info(message)
            raise GitServerError(message)


    def _checkout_branch(self, branch):
        """Checks out the branch.

        Args:
            branch (str): Branch to checkout

        Raises:
            GitServerError: If fails to checkout branch

        """
        try:
            if call(['git', 'fetch', 'origin'], cwd=self.git_dir) == 0:
                if call(['git', 'checkout', branch], cwd=self.git_dir) == 0:
                    return
                message = "Checkout branch failed"
            else:
                message = "Git fetch failed"

        except subprocess.CalledProcessError:
            message = 'Git command exception checking out branch'

        self.release()
        engine.logger.info(message)
        raise GitServerError(message)


    def read_commit_list(self, parent_branch, commit):
        """Reads the list of commits for the branch.

        The list contains the commits since the branch was last branched from or
        merged with the parent branch up to the specified commit.

        Args:
            parent_branch (str): Parent/target branch
            commit (str): SHA of revision

        Raises:
            GitServerError: If Git command error

        """
        commit_list = []
        try:
            raw_commits = check_output(['git', 'rev-list', 'origin/{}..{}'
                                        .format(parent_branch, commit)],
                                       cwd=self.git_dir)
            commit_list = raw_commits.decode().splitlines()

        except subprocess.CalledProcessError:
            pass

        return commit_list


    def read_merge_list(self, commit):
        """Reads the list of merge commits for the branch up to the specified commit.

        Args:
            commit (str): SHA of revision

        """
        commit_list = []
        try:
            raw_commits = check_output(['git', 'rev-list', '--merges', '--first-parent',
                                        commit], cwd=self.git_dir)
            commit_list = raw_commits.decode().splitlines()

        except subprocess.CalledProcessError:
            pass

        return commit_list


    def read_note(self, evidence_type, commit):
        """Reads note for the specified evidence_type and commit.

        Args:
            evidence_type (str): Evidence type to read
            commit (str): SHA of revision

        Returns:
            Note or None if no note present

        """
        try:
            raw_notes = check_output(['git', 'notes', '--ref', evidence_type, 'show', commit],
                                     cwd=self.git_dir)
            return raw_notes
        except subprocess.CalledProcessError:
            # no note present
            return None


    def write_note(self, evidence_type, commit, note):
        """Writes note for the specified evidence_type and commit.

        Args:
            evidence_type (str): Evidence type to read
            commit (str): SHA of revision
            note (str): Note to write

        Raises:
            GitServerError: If Git command error

        """
        try:
            if call(['git', 'notes', '--ref', evidence_type, 'append', '-m', note, commit],
                    cwd=self.git_dir) != 0:
                self.release()
                message = 'Failed to write note'
                engine.logger.info(message)
                raise GitServerError(message)

        except subprocess.CalledProcessError:
            self.release()
            message = 'Exception writing note'
            engine.logger.info(message)
            raise GitServerError(message)


    def push_notes(self, evidence_type):
        """Pushes notes to the remote.

        Args:
            evidence_type (str): Evidence type to read

        Raises:
            GitServerError: If Git command error

        """
        try:
            if call(['git', 'push', 'origin', 'refs/notes/{et}:refs/notes/{et}'.
                     format(et=evidence_type)], cwd=self.git_dir) != 0:
                if call(['git', 'push', '--force', 'origin', 'refs/notes/{et}:refs/notes/{et}'.
                         format(et=evidence_type)], cwd=self.git_dir) != 0:
                    self.release()
                    message = 'Failed to push notes to remote'
                    engine.logger.info(message)
                    raise GitServerError(message)

        except subprocess.CalledProcessError:
            self.release()
            message = 'Exception pushing notes to remote'
            engine.logger.info(message)
            raise GitServerError(message)
