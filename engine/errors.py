class PolicyError(Exception):
    """Represents a policy compliance check failure error."""

    def __init__(self, message):
        self.message = message
        Exception.__init__(self)

    def to_dict(self):
        result = dict()
        result['message'] = self.message
        return result


class GitServerError(Exception):
    """Represents a Git server error."""

    def __init__(self, message):
        self.message = message
        Exception.__init__(self)

    def to_dict(self):
        result = dict()
        result['message'] = self.message
        return result


class ValidationError(Exception):
    """Represents a data validation error."""

    def __init__(self, message):
        self.message = message
        Exception.__init__(self)

    def to_dict(self):
        result = dict()
        result['message'] = self.message
        return result
