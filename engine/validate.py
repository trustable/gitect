import json
import os
import jsonschema
from jsonschema import validate
from engine.errors import ValidationError
import engine.config

def validate_evidence_type(evidence_type):
    """Validates the specified evidence type.

    Args:
        evidence_type (str): Type of evidence

    Raises:
        ValidationError: If evidence type is not supported

    """

    if not evidence_type in engine.config.get('EVIDENCE_TYPES'):
        message = "Evidence type is not supported."
        engine.logger.info(message)
        raise ValidationError(message)


def _validate_data_against_schema(evidence_type, data, is_collection):
    """Validates evidence data.

    Validates data against the schema for the specified evidence type.

    Args:
        evidence_type (str): Type of evidence
        data: Evidence data
        is_collection (bool): True for an evidence collection, False for an
            evidence list

    Raises:
        ValidationError: If evidence data does not comply with schema

    """

    project_path = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))

    schema_path = 'schema/{}-evidence{}.schema.json'
    collection_marker = '-collection' if is_collection else ''
    with open((os.path.join(project_path, schema_path))
              .format(evidence_type, collection_marker), 'r') as schema_file:
        schema = json.load(schema_file)
    try:
        resolver = jsonschema.RefResolver('file://{}/'.format(os.path.join(project_path, 'schema')),
                                          None)
        validate(data, schema, resolver=resolver)
    except jsonschema.exceptions.ValidationError as ex:
        engine.logger.info(ex.message)
        raise ValidationError(ex.message)


def validate_evidence_collection(evidence_type, collection):
    """Validates evidence collection data.

    Wrapper function for validation of evidence collection data. Validates
    data against the schema for the specified evidence type.

    Args:
        evidence_type (str): Type of evidence
        collection: Evidence collection data

    Raises:
        ValidationError: If evidence data does not comply with schema

    """

    _validate_data_against_schema(evidence_type, collection, True)


def validate_evidence_list(evidence_type, evidence_list):
    """Validates evidence list data.

    Wrapper function for validation of evidence list data. Validates
    data against the schema for the specified evidence type.

    Args:
        evidence_type (str): Type of evidence
        evidence_list: Evidence list data

    Raises:
        ValidationError: If evidence data does not comply with schema

    """

    if not evidence_list:
        message = "Evidence list is empty."
        raise ValidationError(message)

    for evidence in evidence_list:
        _validate_data_against_schema(evidence_type, evidence, False)
