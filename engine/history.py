from engine.evidence import read_evidence
from engine.git_handler import GitHandler
from engine.errors import GitServerError
import engine.config

def get_history(host, project, username, password, branch, commit):
    """Gets history of branch up to commit.

    Reads the list of merge commits on the branch, then reads all the evidence for each merge
    commit.

    Args:
        host (str): FQDN of Git host
        project (str): Project path with namespace
        username (str): Service account username
        password (str): Service account access token
        branch (str): Source branch
        commit (str): SHA of revision to retrieve history up to

    Returns:
        History List

    Raises:
        GitServerError: If Git server error

    """
    history_list = []
    git_handler = GitHandler()
    git_handler.open(host, project, username, password, branch)
    merge_commit_list = git_handler.read_merge_list(commit)

    for merge_commit in merge_commit_list:
        history = {}
        history["commit"] = merge_commit
        history["evidence"] = []
        try:
            evidence_list = read_evidence(host,
                                          project,
                                          username,
                                          password,
                                          branch,
                                          merge_commit,
                                          engine.config.get('EVIDENCE_TYPES'),
                                          git_handler)
        except GitServerError:
            git_handler.release()
            raise
        if evidence_list:
            history["evidence"].extend(evidence_list)
        history_list.append(history)

    git_handler.release()
    return history_list
