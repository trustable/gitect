import gitlab
import engine.config
from engine.errors import GitServerError, PolicyError

def read_merge_request(host, project, password, branch, target_branch, merged):
    """Reads details of the merge request from the GitLab server.

    Queries the GitLab server via the GitLab API and retrieves details of the
    matching merge request.

    Args:
        host (str): FQDN of GitLab host
        project (str): Project path with namespace
        password (str): Service account access token
        branch (str): Source branch for merge
        target_branch (str): Target branch for merge
        merged (bool): True for merged MR, False for open MR

    Raises:
        GitServerError: If GitLab API raises an exception
        PolicyError: If no matching merge request is found

    """
    if merged:
        # state is merged in merge webhook, locked in post-receive githook
        state_list = ['locked', 'merged']
    else:
        state_list = ['opened']

    content = {}
    content['merge_comments'] = []

    try:
        gitlab_client = gitlab.Gitlab(
            '{}://{}'.format(engine.config.get('PROTOCOL', 'https'), host),
            private_token=password)

        gl_project = gitlab_client.projects.get(project)
        content['project_id'] = gl_project.id
        merge_requests = [x for x in gl_project.mergerequests.list(all=True)
                          if x.source_branch == branch
                          and x.target_branch == target_branch
                          and x.state in state_list]
        if not merge_requests:
            message = 'Merge request not found.'
            engine.logger.info(message)
            raise PolicyError(message)

        merge_request = merge_requests[0]
        content['sha'] = merge_request.sha
        content['iid'] = merge_request.iid

        mr_comments = merge_request.notes.list(all=True)

        if mr_comments:
            for mr_comment in mr_comments:
                if not mr_comment.system:
                    comment = {}
                    comment['key'] = {}
                    comment['key']['id'] = str(mr_comment.id)
                    comment['key']['patch-set-id'] = str(merge_request.iid)
                    comment['author'] = {}
                    comment['author']['id'] = str(mr_comment.author['id'])
                    comment['author']['username'] = mr_comment.author['username']
                    comment['author']['name'] = mr_comment.author['name']
                    comment['written-on'] = mr_comment.updated_at

                    if mr_comment.resolvable:
                        comment['unresolved'] = not mr_comment.resolved
                    else:
                        comment['unresolved'] = True

                    if hasattr(mr_comment, 'position'):
                        comment['key']['filename'] = mr_comment.position['new_path']
                        comment['line-number'] = mr_comment.position['new_line']
                        comment['rev-id'] = mr_comment.position['head_sha']

                    comment['message'] = mr_comment.body
                    content['merge_comments'].append(comment)

        return content

    except gitlab.exceptions.GitlabError as ex:
        engine.logger.info('Gitlab API Error: %s', ex)
        # ensure password is masked in the exception mesage
        raise GitServerError(ex.error_message.replace(password, '********'))


def read_merge_request_approvals(host, password, project_id, mr_iid):
    """Reads merge request approvals from the GitLab EE server.

    Queries the GitLab server via the GitLab API and retrieves details of the
    matching merge request approvals.

    Args:
        host (str): FQDN of GitLab host
        password (str): Service account access token
        project_id (str): Project id of project
        mr_iid (str): Merge request iid of merge request

    Raises:
        GitServerError: If GitLab API raises an exception

    """
    try:
        gitlab_client = gitlab.Gitlab(
            '{}://{}'.format(engine.config.get('PROTOCOL', 'https'), host),
            private_token=password)

        approvals = (gitlab_client.projects.get(project_id).mergerequests
                     .get(mr_iid).approvals.get())

        approvers = []
        for approver in approvals.approved_by:
            approvers.append(approver.get('user').get('username'))

        return approvers

    except gitlab.exceptions.GitlabError as ex:
        engine.logger.info('Gitlab API Error: %s', ex)
        # ensure password is masked in the exception mesage
        raise GitServerError(ex.error_message.replace(password, '********'))
