import os
import json
import logging
import sys
import engine.config
from .evidence import read_evidence
from .evidence import write_evidence
from .policy import check_policy_compliance
from .policy import merge
from .history import get_history
from .errors import *

# pylint: disable=invalid-name
config = {}

logger = logging.getLogger('gitect')
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler(sys.stdout))

root_dir = os.path.dirname(os.path.dirname(os.path.normpath(__file__)))

with open('{}/app/config.json'.format(root_dir)) as config_file:
    config = json.load(config_file)
try:
    with open('{}/instance/config.json'.format(root_dir)) as config_file:
        config.update(json.load(config_file))
except FileNotFoundError as ex:
    logger.info(str(ex))
try:
    with open('{}/instance/secrets.json'.format(root_dir)) as secrets_file:
        config.update(json.load(secrets_file))
except FileNotFoundError as ex:
    logger.info(str(ex))
