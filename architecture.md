# **gitect** Software Architecture

## **gitect** System Context Diagram

The system context for **gitect** is illustrated in the diagram:

```mermaid
graph TD

subgraph History Clients
    History_Clients[Owner / Lead]
end

subgraph Evidence Clients
    Evidence_Clients[Gate / Orchestrator /<br>Constructor / Validator / <br>Artefact Tracker]
end

subgraph Gate
    Gate_[Gate]
end

subgraph Policy Handler: Git Server
    Git_Server_Policy_Handler[Git Server]
end

subgraph Reviewer
    Reviewer_[Reviewer]
end

subgraph gitect - Evidence Collection System

    subgraph Resource Tracker: gitect
        History_API[History API]
    end

    subgraph Evidence Tracker: gitect
        Evidence_API[Evidence API]
    end

    subgraph Policy Handler: gitect
        Policy_Compliance_API[Policy Compliance API]
        Merges_API[Merges API]
    end

end

subgraph gitect - Client Interface

    subgraph Artefact Attestation Interface
        Git_Attest[git-attest]
    end

end

subgraph Evidence Tracker: Git Server
    Git_Server_Evidence_Tracker[Git Server]
end

History_Clients --> History_API
Evidence_Clients --> Evidence_API
Gate_ --> Policy_Compliance_API
Git_Server_Policy_Handler --> Merges_API
Reviewer_ --> Git_Attest

History_API --> Git_Server_Evidence_Tracker
Evidence_API --> Git_Server_Evidence_Tracker
Policy_Compliance_API --> Git_Server_Evidence_Tracker
Merges_API --> Git_Server_Evidence_Tracker
Git_Attest --> Git_Server_Evidence_Tracker
```

## Evidence Storage

**gitect** stores all evidence in Git notes in the main project repository.
Git notes are a mechanism to store supplementary data associated with a Git commit.
For more details about Git notes refer to <https://git-scm.com/docs/git-notes>.

The advantage of storing the evidence data in Git notes is that the evidence is
stored together with the source code, tightly bound to the actual commits to
which it relates. If the repository is mirrored, moved, archived, etc. then the
evidence is retained as an intrinsic part of the software repository. As a core
feature of Git, Git notes are available across the full ecosystem of Git tools
and servers.

Each evidence type is saved to its own notes ref type. Multiple notes may be
stored for each commit, they are appended and stored as a single text block. The
evidence data itself is encoded in JSON format as defined in the evidence data
[schemas](app/schema).

## Evidence Collection System

The Evidence Collection System comprises the gitect REST API and the gitect
Engine.

### Architecture Diagram

The architecture of the Evidence Collection System is illustrated in the diagram:

```mermaid
graph TD

subgraph gitect - Evidence Collection System

    subgraph gitect REST API
        History_API[History API]
        Evidence_API[Evidence API]
        Policy_Compliance_API[Policy Compliance API]
        Merges_API[Merges API]
    end

    subgraph gitect Engine
        subgraph gitect Engine API
            History[History]
            Policy[Policy]
            Evidence[Evidence]
        end

        subgraph Git Server Interface
            Git_Handler[Git Handler]
            Gitlab_Adapter[GitLab Adapter]
        end
    end
end

History_API --> |Get History| History
Evidence_API --> |Read/Write Evidence| Evidence
Policy_Compliance_API --> |Check Policy Compliance| Policy
Merges_API --> |Merge| Policy
Policy --> Evidence
History --> Git_Handler
Evidence --> Git_Handler
History --> Evidence
Policy --> Git_Handler
Policy --> Gitlab_Adapter
```

### gitect REST API

The gitect API provides a REST API for client interaction with **gitect**.

The gitect REST API is implemented using the [Flask](http://flask.pocoo.org/)
microframework for Python.

Using this API there is a minimal environment requirement on the client side tool.
Typically a call to [curl](https://curl.haxx.se/) from a bash script could be
used or similar bindings are available in other scripting environments that may
be in use within the tools.

There is a need for storage of credentials for authentication with the API. There
should be a mechanism for secrets management available within the environment that
is already being used to authenticate with other tools within the toolchain (e.g.
Change Tracker, Artefact Tracker, etc.).

### gitect Engine

The gitect Engine implements the internal application logic.

#### **gitect** Engine API

The gitect Engine API is used by the gitect REST API and also provides an
alternative interface for direct client interaction with **gitect**.

#### Git Server Interface

The Git Server Interface comprises the Git Handler component and the GitLab
Adapter component. The Git Handler is responsible for maintaining clones of the
Git repositories and executing all Git commands required for management of the
evidence. The GitLab Adapter is responsible for executing all GitLab specific
operations via the GitLab API.

## Client Interface

The Client Interface consists of a single component, the Artefact Attestation
Interface. This is implemented as a custom Git command.

The purpose of the Artefact Attestation Interface is to provide a mechanism for
the Reviewer to attest to the provenance of any external artefacts used
in the software.

The Artefact Attestation Interface is implemented as a Bash shell script.
