#!/bin/bash

# NGINX setup script
# Must be run from the gitect repo's root directory

# This must match the NGINX root directory
mkdir -p /var/www/html/git
chown -R www-data:www-data /var/www/html/git
# Copy our custom NGINX config to its destination
cd /etc/nginx/sites-available || exit 1
mv default default.old
cd - || exit 1
cp tests/config/default /etc/nginx/sites-available

htpasswd -b -c /var/www/html/git/htpasswd "${NGINX_LOGIN}" "${NGINX_PASSWORD}"
nginx -t
/etc/init.d/fcgiwrap restart
/etc/init.d/nginx restart
mkdir -p instance
cp tests/config/instance/config.json instance/
cp tests/config/instance/secrets.json instance/
sed -i "s/<insert_username>/gitect-service/
        s/<insert_host_token>/${GITECT_HOST_TOKEN}/
        s/<insert_service_token>/${GITECT_SERVICE_TOKEN}/" instance/secrets.json
