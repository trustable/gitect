import pytest
from _pytest.runner import runtestprotocol

import engine

@pytest.fixture(scope="session", autouse=True)
def set_engine_config():
    """Edit config needed by the engine.
       This is important as we need to use non-default values in our test
       environment.
    """

    engine.config.update(PROTOCOL="http")
    engine.config.update(DATA_DIR="/tmp")


def pytest_addoption(parser):
    parser.addoption("--login", action="append", default=[])
    parser.addoption("--password", action="append", default=[])


def pytest_generate_tests(metafunc):
    # Called for every test

    if "login" in metafunc.fixturenames and "password" in metafunc.fixturenames:
        metafunc.parametrize("login", metafunc.config.getoption("login"))
        metafunc.parametrize("password", metafunc.config.getoption("password"))


def pytest_runtest_protocol(item, nextitem):
    reports = runtestprotocol(item, nextitem)
    for report in reports:
        if report.when == "call":
            print("\n%s : %s" % (item.name.split('[')[0], report.outcome))
