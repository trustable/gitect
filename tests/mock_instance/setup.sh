#!/bin/bash

# Mock Gitlab server configuration and startup script

export LC_ALL=C.UTF-8
export LANG=C.UTF-8
cd tests/mock_instance/app || exit 1
# gunicorn will listen on a newly created socket, handling specific requests
# and the corresponding responses between NGINX and the mock Gitlab server
gunicorn --workers 3 --bind unix:/tmp/mock_app.sock -m 777 wsgi:app &
FLASK_APP=app.py flask run &
cd - || exit 1
