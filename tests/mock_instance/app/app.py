import os
import subprocess
from subprocess import check_output

from flask import Flask, Response

# pylint: disable=invalid-name
app = Flask(__name__)

branch_mr_ids = {"test_branch": 1,
                 "test_branch_with_approvals": 2,
                 "test_branch_with_approvals_no_unique_approver": 3,
                 "test_branch1" : 4,
                 "test_branch2" : 5,
                 "test_branch3" : 6,
                 "test_branch_merged" : 7,
                 "test_branch_locked" : 8}

branch_states = {"test_branch": 'opened',
                 "test_branch_with_approvals": 'opened',
                 "test_branch_with_approvals_no_unique_approver": 'opened',
                 "test_branch1" : 'merged',
                 "test_branch2" : 'merged',
                 "test_branch3" : 'merged',
                 "test_branch_merged" : 'merged',
                 "test_branch_locked" : 'locked'}


# pylint: disable=line-too-long,unused-argument
@app.route('/api/v4/projects/<path:project_id>/merge_requests', methods=['GET'])
def merge_requests(project_id):
    # We need merge requests for several branches, with various levels of
    # policy compliance

    mr = '{"id":13141685,"iid":%(mr_id)s,"project_id":6322673,"title":"Engine API","description":"Test MR description","state":"%(state)s","created_at":"2018-06-20T09:36:03.389Z","updated_at":"2018-06-21T13:33:58.558Z","target_branch":"master","source_branch":"%(source_branch)s","upvotes":0,"downvotes":0,"author":{"id":2149165,"name":"User 2","username":"user2","state":"active","avatar_url":"https://secure.gravatar.com/avatar/47da706601c87684a0cde5ba7311261f?s=80\u0026d=identicon","web_url":"http://localhost:4242/user2"},"assignee":null,"source_project_id":6322673,"target_project_id":6322673,"labels":[],"work_in_progress":false,"milestone":null,"merge_when_pipeline_succeeds":false,"merge_status":"can_be_merged","sha":"%(feature_sha)s","merge_commit_sha":"%(target_sha)s","user_notes_count":4,"discussion_locked":null,"should_remove_source_branch":null,"force_remove_source_branch":false,"web_url":"http://localhost:4242/trustable/gitect/merge_requests/21","time_stats":{"time_estimate":0,"total_time_spent":0,"human_time_estimate":null,"human_total_time_spent":null},"squash":false,"subscribed":true,"changes_count":"12","merged_by":{"id":23209,"name":"user3","username":"user3","state":"active","avatar_url":"https://secure.gravatar.com/avatar/739d919902d16a01d23ed08815568415?s=80\u0026d=identicon","web_url":"http://localhost:4242/user3"},"merged_at":"2018-06-21T13:31:23.704Z","closed_by":null,"closed_at":null,"latest_build_started_at":"2018-06-21T11:24:43.403Z","latest_build_finished_at":"2018-06-21T11:30:36.784Z","first_deployed_to_production_at":null,"pipeline":{"id":24295223,"sha":"%(feature_sha)s","ref":"engine-api","status":"success"},"diff_refs":{"base_sha":"293378da7fed1346955f877c0013c3a4501dc352","head_sha":"%(feature_sha)s","start_sha":"293378da7fed1346955f877c0013c3a4501dc352"},"approvals_before_merge":null}'

    # The SHAs of both the feature and the target branch need to be retrieved
    # as they cannot be set statically
    cwd = os.getcwd()
    os.chdir("/var/www/html/git/test_evidence_collection/remote.git/")

    target_branch_head = check_output(['git', 'rev-parse', 'master']).strip().decode("utf-8")

    mrs = []
    for test_branch in branch_mr_ids:
        try:
            test_branch_head = check_output(['git', 'rev-parse', test_branch],
                                            stderr=subprocess.STDOUT).strip().decode("utf-8")
            mrs.append(mr % dict(state=branch_states[test_branch],
                                 source_branch=test_branch,
                                 feature_sha=test_branch_head,
                                 target_sha=target_branch_head,
                                 mr_id=branch_mr_ids[test_branch]))
        except subprocess.CalledProcessError:
            # do not add the MR to the response if the corresponding feature branch does not exist
            pass

    os.chdir(cwd)

    return Response(response="[" + ", ".join(branch_mr for branch_mr in mrs) + "]",
                    status=200,
                    mimetype="application/json")

@app.route('/api/v4/projects/<path:project_id>/merge_requests/<int:mr_id>/approvals', methods=['GET'])
def mr_approvals(project_id, mr_id):
    response_json = '{"id":14476696,"iid":48,"project_id":6322673,"title":"Improve artefact attestation tests for local runs","description":"In the same vein as !47, this containerises artefact attestation tests, which, along with said merge request, fixes #20.","state":"merged","created_at":"2018-07-26T16:08:28.577Z","updated_at":"2018-08-16T07:19:38.274Z","merge_status":"can_be_merged","approvals_required":1,"approvals_left":0,"approved_by":[{"user":{"id":2149165,"name":"User 2","username":"user2","state":"active","avatar_url":"https://secure.gravatar.com/avatar/47da706601c87684a0cde5ba7311261f?s=80\u0026d=identicon","web_url":"https://gitlab.com/user2"}}],"suggested_approvers":[{"id":23209,"name":"user3","username":"user3","state":"active","avatar_url":"https://secure.gravatar.com/avatar/739d919902d16a01d23ed08815568415?s=80\u0026d=identicon","web_url":"https://gitlab.com/user3"}],"approvers":[{"user":{"id":2149165,"name":"User 2","username":"user2","state":"active","avatar_url":"https://secure.gravatar.com/avatar/47da706601c87684a0cde5ba7311261f?s=80\u0026d=identicon","web_url":"https://gitlab.com/user2"}},{"user":{"id":23209,"name":"user3","username":"user3","state":"active","avatar_url":"https://secure.gravatar.com/avatar/739d919902d16a01d23ed08815568415?s=80\u0026d=identicon","web_url":"https://gitlab.com/user3"}}],"approver_groups":[],"user_has_approved":false,"user_can_approve":false}'
    return Response(response=response_json, status=200, mimetype="application/json")

@app.route('/api/v4/projects/<path:project_id>/merge_requests/<int:mr_id>/notes', methods=['GET'])
def mr_notes(project_id, mr_id):
    response_no_approval = '[{"id":94827346,"type":null,"body":"The validation-results-list field is defined as a list of references to change validation output URIs. In this case the change validation results are stored in the comments field so there is no external results URL to reference.","attachment":null,"author":{"id":2149165,"name":"User 2","username":"user2","state":"active","avatar_url":"https://secure.gravatar.com/avatar/47da706601c87684a0cde5ba7311261f?s=80\u0026d=identicon","web_url":"http://localhost:4242/user2"},"created_at":"2018-08-16T07:19:07.443Z","updated_at":"2018-08-16T07:19:07.443Z","system":false,"noteable_id":14664314,"noteable_type":"MergeRequest","resolvable":false,"noteable_iid":50},{"id":94381704,"type":null,"body":"What is the rationale behind this?","attachment":null,"author":{"id":2287080,"name":"User 1","username":"user1","state":"active","avatar_url":"https://secure.gravatar.com/avatar/d41a60b82e621aee6f3fc9a936742ae3?s=80\u0026d=identicon","web_url":"http://localhost:4242/user1"},"created_at":"2018-08-14T10:24:41.077Z","updated_at":"2018-08-14T10:24:41.077Z","system":false,"noteable_id":14664314,"noteable_type":"MergeRequest","resolvable":false,"noteable_iid":50}]'
    response_with_approvals = '[{"id":94827346,"type":null,"body":"APPROVED. This is a test comment.","attachment":null,"author":{"id":2149165,"name":"User 2","username":"user2","state":"active","avatar_url":"https://secure.gravatar.com/avatar/47da706601c87684a0cde5ba7311261f?s=80\u0026d=identicon","web_url":"http://localhost:4242/user2"},"created_at":"2018-08-16T07:19:07.443Z","updated_at":"2018-08-16T07:19:07.443Z","system":false,"noteable_id":14664314,"noteable_type":"MergeRequest","resolvable":false,"noteable_iid":50},{"id":94381704,"type":null,"body":"APPROVED","attachment":null,"author":{"id":2287080,"name":"User 1","username":"user1","state":"active","avatar_url":"https://secure.gravatar.com/avatar/d41a60b82e621aee6f3fc9a936742ae3?s=80\u0026d=identicon","web_url":"http://localhost:4242/user1"},"created_at":"2018-08-14T10:24:41.077Z","updated_at":"2018-08-14T10:24:41.077Z","system":false,"noteable_id":14664314,"noteable_type":"MergeRequest","resolvable":false,"noteable_iid":50}]'
    response_with_approvals_non_unique_approver = '[{"id":94827346,"type":null,"body":"APPROVED. This is a test comment.","attachment":null,"author":{"id":2149165,"name":"User 2","username":"user2","state":"active","avatar_url":"https://secure.gravatar.com/avatar/47da706601c87684a0cde5ba7311261f?s=80\u0026d=identicon","web_url":"http://localhost:4242/user2"},"created_at":"2018-08-16T07:19:07.443Z","updated_at":"2018-08-16T07:19:07.443Z","system":false,"noteable_id":14664314,"noteable_type":"MergeRequest","resolvable":false,"noteable_iid":50},{"id":94381704,"type":null,"body":"APPROVED","attachment":null,"author":{"id":2149165,"name":"User 2","username":"user2","state":"active","avatar_url":"https://secure.gravatar.com/avatar/47da706601c87684a0cde5ba7311261f?s=80\u0026d=identicon","web_url":"http://localhost:4242/user2"},"created_at":"2018-08-14T10:24:41.077Z","updated_at":"2018-08-14T10:24:41.077Z","system":false,"noteable_id":14664314,"noteable_type":"MergeRequest","resolvable":false,"noteable_iid":50}]'

    notes_by_mr = {branch_mr_ids["test_branch"] : response_no_approval,
                   branch_mr_ids["test_branch_with_approvals"] : response_with_approvals,
                   branch_mr_ids["test_branch_with_approvals_no_unique_approver"] : response_with_approvals_non_unique_approver,
                   # Approvals are not relevant to these branches, used simply to test history reads
                   branch_mr_ids["test_branch1"] : response_no_approval,
                   branch_mr_ids["test_branch2"] : response_no_approval,
                   branch_mr_ids["test_branch3"] : response_no_approval,
                   branch_mr_ids["test_branch_merged"] : response_no_approval,
                   branch_mr_ids["test_branch_locked"] : response_no_approval}

    return Response(response=notes_by_mr[mr_id], status=200, mimetype="application/json")

@app.route('/api/v4/projects/<path:project_id>', methods=['GET'])
def project(project_id):
    response_json = '{"id":6322673,"description":"Git Evidence Collection Tool: A proof-of-concept tool for a trustable software development workflow","name":"gitect","name_with_namespace":"trustable / gitect","path":"gitect","path_with_namespace":"trustable/gitect","created_at":"2018-05-09T05:49:00.063Z","default_branch":"master","tag_list":[],"ssh_url_to_repo":"git@gitlab.com:trustable/gitect.git","http_url_to_repo":"http://localhost:4242/trustable/gitect.git","web_url":"http://localhost:4242/trustable/gitect","readme_url":"http://localhost:4242/trustable/gitect/blob/master/README.md","avatar_url":null,"star_count":1,"forks_count":0,"last_activity_at":"2018-08-15T11:19:47.057Z","namespace":{"id":879086,"name":"trustable","path":"trustable","kind":"group","full_path":"trustable","parent_id":null},"_links":{"self":"http://localhost:4242/api/v4/projects/6322673","issues":"http://localhost:4242/api/v4/projects/6322673/issues","merge_requests":"http://localhost:4242/api/v4/projects/6322673/merge_requests","repo_branches":"http://localhost:4242/api/v4/projects/6322673/repository/branches","labels":"http://localhost:4242/api/v4/projects/6322673/labels","events":"http://localhost:4242/api/v4/projects/6322673/events","members":"http://localhost:4242/api/v4/projects/6322673/members"},"archived":false,"visibility":"public","resolve_outdated_diff_discussions":false,"container_registry_enabled":true,"issues_enabled":true,"merge_requests_enabled":true,"wiki_enabled":true,"jobs_enabled":true,"snippets_enabled":true,"shared_runners_enabled":true,"lfs_enabled":false,"creator_id":2149165,"import_status":"none","import_error":null,"open_issues_count":13,"runners_token":"ZXoQ51X8muyQpayqnPV8","public_jobs":true,"ci_config_path":null,"shared_with_groups":[],"only_allow_merge_if_pipeline_succeeds":false,"request_access_enabled":true,"only_allow_merge_if_all_discussions_are_resolved":false,"printing_merge_request_link_enabled":true,"merge_method":"rebase_merge","permissions":{"project_access":{"access_level":40,"notification_level":3},"group_access":{"access_level":50,"notification_level":3}},"approvals_before_merge":1,"mirror":false}'
    return Response(response=response_json, status=200, mimetype="application/json")
