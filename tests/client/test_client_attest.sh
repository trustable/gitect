#!/bin/bash

# Idea: create a temporary/test repo to push attestations to, in order to avoid
# polluting the gitect repo itself with rubbish data

if [ $# -ge 1 ]; then
    branch_name=$1 # when called by the CI, should be set to the pipeline ID to prevent conflicts
fi

if [ -z "$branch_name" ]; then
    echo "Usage: $0 <branch_name>"
    exit 1
fi

# Use `git config` as `git config --local` to avoid relying on system- or even
# user-wide git config, as we are playing with custom configurations here.
# Note: `git` is here aliased with a trailing space for the next word, namely
# `config` in our case, to be checked for alias substitution, as explained in
# the bash manual.
shopt -s expand_aliases
alias git='git '
alias config='config --local'

attestations_tag_name=ARTEFACTS_ATTEST
nb_errors=0
test_dir=$(pwd)/test_attestation

# configure the name and email to use in git commits
configure_git_identity() {
    if ! git config --get user.email >/dev/null; then
        git config user.email "em@il.com";
    fi
    if ! git config --get user.name >/dev/null; then
        git config user.name "Gitect Test CI";
    fi
}

# prepare the test environment, with a fresh clone and a remote on the same
# host.
create_test_environment() {
    mkdir -p "$test_dir"/clone
    cd "$test_dir"/clone || exit 1
    git init
    configure_git_identity

    cd ..
    git clone --mirror clone remote/

    cd clone || exit 1
    git remote add origin ../remote

    # Create a first commit to initiate the remote
    echo 'This is a test file' > test_file1
    git add test_file1
    git commit -m 'Initial commit'
    git push origin master
}


# create a temporary commit on a new branch for testing purposes
create_commit() {
    git checkout -b "$branch_name"
    echo 'This is another test file' > test_file2
    git add test_file2
    git commit -m 'Add test file'
}

assert() {
    true_or_false="$2"

    wrong_usage=0
    if [ $# != 2 ] || [ "$true_or_false" != 'true' ] && [ "$true_or_false" != 'false' ]; then
        wrong_usage=1
    fi
    if [ $wrong_usage -eq 1 ]; then
        echo 'Usage assert <expression> <true|false>'
    fi

    if [ "$true_or_false" == 'true' ]; then
        expected_result=0
    else
        expected_result=1
    fi

    eval "$1"
    rc=$?
    #echo "return code: $rc"
    if [ $rc == 0 ] && [ $expected_result != 0 ]; then
        echo "${FUNCNAME[1]}: Assertion failed: \"$1\" was expected to fail"
        ((nb_errors++))
        return
    fi
    if [ $rc != 0 ] && [ $expected_result == 0 ]; then
        echo "${FUNCNAME[1]}: Assertion failed: \"$1\" was expected to succeed"
        ((nb_errors++))
        return
    fi
    echo "${FUNCNAME[1]}: Assertion successful: \"$1\" \"$true_or_false\""
}


# test that `git assert init` works as expected
test_init() {
    # Check there is no attestation in the initial state
    attestations=$(git attest show 2>/dev/null)
    assert "test -z $attestations" true

    # Check that the attestations tag doesn't exist
    assert "test -z $(git tag -l $attestations_tag_name)" true

    # Check that `git attest init` works
    assert "git attest init" true

    # Check that the attestations tag now exists
    assert "test -z $(git tag -l $attestations_tag_name)" false
}

# test that attestations have the right properties with relevant values
test_read_attestation_contents() {
    assert "jq '.' url_output" true
    assert "test $(jq '."id"' url_output) = \"$url\"" true
    # shellcheck disable=2086
    assert "test $(jq '."artefact-name"' url_output) = \"$(basename $url)\"" true
    assert "test $(jq '."comment"' url_output) = \"$comment\"" true
    git_username=$(git config --get user.name)
    git_email=$(git config --get user.email)
    assert "test $(jq '."contributor-id"' url_output) = \"$git_username <$git_email>\"" true

    # Check that the attestation date is recent enough (assuming it to be
    # within the same second as the current date will not always work; a few
    # seconds are safe enough)
    attestation_datetime=$(jq '."date-time"' url_output)
    attestation_seconds=$(date --date="${attestation_datetime//\"/}" +%s)
    now_seconds=$(date +%s)
    assert "test $((now_seconds - attestation_seconds)) -lt 5" true
}

# test that attestations are non-empty
test_read_nb_lines() {
    git attest show > full_output
    assert "test $(wc -l full_output | cut -d' ' -f1) -eq 0" false
    # shellcheck disable=2086
    git attest show "$(basename $url)" > url_output
    assert "test $(wc -l url_output | cut -d' ' -f1) -eq 0" false
}

# test that reading non existent attestations returns empty strings
test_read_nonexistent_attestation() {
    assert "test -z $(git attest show this-does-not-exist)" true
    # shellcheck disable=2086
    assert "test -z $(git attest show this-does-not-exist $url)" false
}

# test writes with various combinations of arguments
test_write_arguments() {
    # Check that a missing message or URL results in an error
    assert "git attest add -m 'hello'" false
    # XXX: the message argument is currently optional
    # (if not provided, the preferred editor is opened for a message to be input)
    echo 'Hello!
This is a non-interactively written attestation message.' > attestation_"$(date +%s)"
    git config core.editor : # the file is ready, no need to invoke an editor

    url_editor_msg="${url/gz/xz}"
    assert "git attest add -u $url_editor_msg" true
    comment='hello'
    assert "git attest add -u $url -m $comment" true
}

# test that attestations can be written and read properly
test_read_write() {
    create_commit

    url=https://example.com/path/to/file.tar.gz

    # shellcheck disable=SC2046
    assert "test -z $(git attest show $(basename $url))" true

    test_write_arguments

    test_read_nonexistent_attestation

    test_read_nb_lines

    test_read_attestation_contents
}

# test the impossibility to write artefact attestations in an environment which
# does not have any git identity configured
test_blank_git_identity() {
    create_test_environment
    git attest init

    git config --unset user.name
    git config --unset user.email

    assert "git attest add -u 'foo' -m 'bar'" false
    # Check that no attestation has been written
    assert "test -z $(git attest show 2>/dev/null)" true

    configure_git_identity
    assert "git attest add -u 'foo' -m 'bar'" true
    # Check that an attestation has been written
    assert "test -z $(git attest show 2>/dev/null)" false

    destroy_test_environment
}

# test configuration changes applied by the gitect client on various initial
# git configurations
test_implicit_push_config() {
    notes_ref='refs/notes/*:refs/notes/*'
    notes_ref_escaped='refs/notes/\*:refs/notes/\*'

    # check that configuration which initially contained the notes reference is
    # left with a single occurrence of it
    create_test_environment
    git config --add remote.origin.push "$notes_ref"
    git attest init
    # shellcheck disable=2086
    assert "test $(git config --get-all remote.origin.push | grep -c $notes_ref_escaped) = 1" true
    destroy_test_environment

    # same for configuration which did not initially contain the notes reference
    create_test_environment
    git attest init
    # shellcheck disable=2086
    assert "test $(git config --get-all remote.origin.push | grep -c $notes_ref_escaped) = 1" true
    destroy_test_environment

    # check that configuration which initially contained a reference unrelated
    # to the notes still has it after the notes reference is added
    create_test_environment
    git config --add remote.origin.push "foo"
    git attest init
    expected_config="foo
$notes_ref"
    assert "test \"$(git config --get-all remote.origin.push)\" = \"$expected_config\"" true
    destroy_test_environment

    # check that configuration which initially contained a reference unrelated
    # to the notes *and* the notes reference is left unchanged
    create_test_environment
    git config --add remote.origin.push "foo"
    git config --add remote.origin.push "$notes_ref"
    git attest init
    expected_config="foo
$notes_ref"
    assert "test \"$(git config --get-all remote.origin.push)\" = \"$expected_config\"" true
    destroy_test_environment
}


# Get rid of the test environment to prevent possible conflicts with future
# runs of the test suite
destroy_test_environment() {
    if [ -d "$test_dir" ]; then
        echo "Deleting $test_dir...."
        rm -rf "$test_dir"
    fi
}


test_implicit_push_config
test_blank_git_identity
create_test_environment
test_init

# Check that there is initially no attestation to show
assert "test -z $(git attest show 2>/dev/null)" true

test_read_write

git config --add remote.origin.push HEAD

remote_attestation_ref="$test_dir/remote/refs/notes/artefact-construction"
test_branch_ref="$test_dir/remote/refs/heads/$branch_name"
# check that neither the test branch nor the attestation-related note
# references exist on the remote
assert "test -e $remote_attestation_ref" false
assert "test -e $test_branch_ref" false
git push
# check that both the test branch and the attestation-related note references
# have been pushed to the remote
assert "test -e $remote_attestation_ref" true
assert "test -e $test_branch_ref" true

# Unalias our previously set alias to prevent possible conflicts with user
# commands in the same shell
unalias config

trap destroy_test_environment EXIT
exit $nb_errors
