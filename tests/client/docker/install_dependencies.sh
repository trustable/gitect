#!/bin/bash

# This script contains all time- and/or bandwidth-consuming commands needed for
# setting up a testing environment, which is intended to be done at container
# build time so tests can be seamlessly repeated locally.
# Note: any operation requiring additional files or directories to be copied or
# mounted from the host will fail here.

apt update
apt install -y jq curl git
