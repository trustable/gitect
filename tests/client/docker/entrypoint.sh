#!/bin/bash

generate_evidence=1
if [ "$1" == "--no-evidence" ]; then
    # Should only be used for local runs, where the variables needed to
    # generate evidence should be undefined anyway
    generate_evidence=0
    branch_name="test-branch"
else
    # Pipeline run
    cd /builds/trustable/gitect || exit 1
    branch_name="${CI_PIPELINE_ID}"
fi

if [ "$generate_evidence" -eq 1 ]; then
    export ATT_TEST_ENVRT_CONSTR_ID="${CI_PIPELINE_ID}_test_attestation_interface"
    # gitect: Generate environment construction evidence

    # shellcheck disable=SC2001
    json_data="$(echo "${GITECT_ENVRT_CONSTR_EVIDENCE}" \
        | sed "s/!!ENVRT_ID!!/${ATT_TEST_ENVRT_CONSTR_ID}/
               s/!!ORCHESTRATOR_ID!!/gitlab-ci:${CI_RUNNER_VERSION}/
               s/!!ENVRT_TYPE!!/docker:/
               s/!!DATE_TIME!!/$(date -Iseconds)/
               s/!!COMMENT!!/Test environment for gitect's artefacts attestation interface/")"
    curl -k -H "Content-Type: application/json" -H "X-Gitect-Token: ${GITECT_HOST_TOKEN}" -d \
        "$json_data" "${GITECT_EVIDENCE_ENDPOINT}/environment-construction"
else
    echo "### Running locally, hence not generating environment construction evidence."
fi

cp client/git-attest /usr/local/bin
tests/client/test_client_attest.sh "$branch_name" | tee tests.log

success_marker='Assertion successful'
failure_marker='Assertion failed'
sed -i 's/"/\\\\"/g;s/\*/\\*/g;s/: /:/g' tests.log
grep -E "($success_marker|$failure_marker):" tests.log > relevant_output

tests_ids='['
global_result=true
while IFS=":" read -r func result _; do

    if [ "$result" == "$failure_marker" ]; then
        global_result=false
    fi

    if [ "${tests_ids/$func}" = "$tests_ids" ]; then
        tests_ids+="\"$func\","
    fi

done < relevant_output

rm -f success
if [ "$global_result" == true ]; then
    # Needed for the pipeline to identify whether the relevant stage has succeeded
    touch success
fi

if [ "$generate_evidence" -eq 1 ]; then
    # gitect Generate change validation evidence including test results

    tests_ids=${tests_ids/%,/]}
    comment=$(<relevant_output)

    # escape newlines
    comment=$(echo "${comment}" | tr '\n' '; ')

    change_validation_id=${CI_PROJECT_PATH_SLUG}_test_attestation_interface_$(date +%s%N)
    mkdir -p "${CHANGE_VALIDATIONS_PATH}"
    echo "export TEST_ATTESTATION_INTERFACE_CHANGE_VALIDATION_ID=$change_validation_id" >> "${CHANGE_VALIDATIONS_PATH}/test_attestation_interface.env"

    # shellcheck disable=SC2001
    json_data="$(echo "${GITECT_CHANGE_VALID_EVIDENCE}" \
        | sed "s/!!CHANGE_VALIDATION_ID!!/$change_validation_id/
               s%!!VALIDATION_RESULTS!!%[]%
               s%!!TESTS_IDS!!%$tests_ids%
               s/!!CHANGE_VALIDATION_TYPE!!/Artefacts attestation interface tests/
               s/!!CHANGE_VALIDATOR_ID!!/test_client_attest.sh:${CI_COMMIT_SHA}/
               s/!!ENVRT_ID!!/${ATT_TEST_ENVRT_CONSTR_ID}/
               s/!!GLOBAL_RESULT!!/${global_result}/
               s/!!DATE_TIME!!/$(date -Iseconds)/
               s%!!COMMENT!!%$comment%")"
    curl -k -H "Content-Type: application/json" -H "X-Gitect-Token: ${GITECT_HOST_TOKEN}" -d \
        "$json_data" "${GITECT_EVIDENCE_ENDPOINT}/change-validation"
else
    echo "### Running locally, hence not generating change validation evidence."
fi
