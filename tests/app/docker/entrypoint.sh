#!/bin/bash

generate_evidence=1
if [ "$1" == "--no-evidence" ]; then
    # Should only be used for local runs, for which the variables needed to
    # generate evidence should be undefined anyway
    generate_evidence=0
else
    # Pipeline run
    cd /builds/trustable/gitect || exit 1
fi

if [ "$generate_evidence" -eq 1 ]; then
    export EV_COL_TEST_ENVRT_CONSTR_ID="${CI_PIPELINE_ID}_test_evidence_collection"
    # gitect: Generate environment construction evidence
    # shellcheck disable=SC2001
    json_data="$(echo "${GITECT_ENVRT_CONSTR_EVIDENCE}" \
        | sed "s/!!ENVRT_ID!!/${EV_COL_TEST_ENVRT_CONSTR_ID}/
               s/!!ORCHESTRATOR_ID!!/gitlab-ci:${CI_RUNNER_VERSION}/
               s/!!ENVRT_TYPE!!/docker:/
               s/!!DATE_TIME!!/$(date -Iseconds)/
               s/!!COMMENT!!/Test environment for gitect's evidence collection REST API/")"
    curl -k -H "Content-Type: application/json" -H "X-Gitect-Token: ${GITECT_HOST_TOKEN}" -d \
        "$json_data" "${GITECT_EVIDENCE_ENDPOINT}/environment-construction"
else
    echo "### Running locally, hence not generating environment construction evidence."
fi

./tests/config/setup_nginx.sh

./tests/mock_instance/setup.sh

# Configure gitect secrets for compatibility with the local git server
cat > instance/secrets.json <<EOF
{
	"HOSTS": {
		"localhost:4242": {
			"username": "${NGINX_LOGIN}",
			"token": "${NGINX_PASSWORD}"
		}
	},
	"TOKEN": "7357H05770k3n"
}
EOF

# Disable HTTPs for containerised testing
cp app/config.json app/config.json.bkp
sed -i 's/https/http/' app/config.json

gunicorn gitect:app -b localhost:8000 &

cp tests/config/conftest.py tests/app/
pytest-3 -q -v --login="${NGINX_LOGIN}" --password="${NGINX_PASSWORD}" tests/app/test_evidence_collection_app.py | tee tests.log

sed -i 's/"/\\\\"/g;s/\*/\\*/g' tests.log
grep -E "^test_.* : (fail|pass)ed$" tests.log > relevant_output

tests_ids='['
global_result=true
while IFS=" : " read -r func result; do
    tests_ids+="\"$func\","
    if [ "$result" == "failed" ]; then
        global_result=false
    fi
done < relevant_output

rm -f success
if [ "$global_result" == true ]; then
    # Needed for the pipeline to identify whether the relevant stage has succeeded
    touch success
fi

if [ "$generate_evidence" -eq 1 ]; then
    # gitect Generate change validation evidence including test results

    tests_ids="${tests_ids%,}]"
    comment=$(<relevant_output)

    # add dependencies versions info
    comment="$(</installed_dependencies);$comment"

    # escape newlines
    comment=$(echo "${comment}" | tr '\n' '; ')

    change_validation_id=${CI_PROJECT_PATH_SLUG}_test_evidence_collection_$(date +%s%N)
    mkdir -p "${CHANGE_VALIDATIONS_PATH}"
    echo "export TEST_EVIDENCE_COLLECTION_APP_CHANGE_VALIDATION_ID=$change_validation_id" >> "${CHANGE_VALIDATIONS_PATH}/test_evidence_collection_app.env"

    pytest_version=$(pytest-3 --version 2>&1 | grep -o 'version .*,' | sed 's/version //; s/,//')
    # shellcheck disable=SC2001
    json_data="$(echo "${GITECT_CHANGE_VALID_EVIDENCE}" \
        | sed "s/!!CHANGE_VALIDATION_ID!!/$change_validation_id/
               s%!!VALIDATION_RESULTS!!%[]%
               s%!!TESTS_IDS!!%$tests_ids%
               s/!!CHANGE_VALIDATION_TYPE!!/Evidence collection REST API tests/
               s/!!CHANGE_VALIDATOR_ID!!/pytest:${pytest_version}/
               s/!!ENVRT_ID!!/${EV_COL_TEST_ENVRT_CONSTR_ID}/
               s/!!GLOBAL_RESULT!!/${global_result}/
               s/!!DATE_TIME!!/$(date -Iseconds)/
               s%!!COMMENT!!%$comment%")"
    curl -k -H "Content-Type: application/json" -H "X-Gitect-Token: ${GITECT_HOST_TOKEN}" -d \
        "$json_data" "${GITECT_EVIDENCE_ENDPOINT}/change-validation"
else
    echo "### Running locally, hence not generating change validation evidence."
fi

# Cleanup
rm relevant_output
rm tests/app/conftest.py
rm -rf tests/app/__pycache__
mv app/config.json.bkp app/config.json
