#!/bin/bash

# This script contains all time- and/or bandwidth-consuming commands needed for
# setting up a testing environment, which is intended to be done at container
# build time so tests can be seamlessly repeated locally.
# Note: any operation requiring additional files or directories to be copied or
# mounted from the host will fail here.

apt update
apt install -y curl git nginx apache2-utils fcgiwrap python3 python3-pip python3-pytest gunicorn

# Configure a Flask server to send mock responses to a specific set of requests
# for testing
pip3 install -r requirements.txt | tee pip3.log

pip_installed_marker="Successfully installed "
grep "$pip_installed_marker" pip3.log | sed "s/$pip_installed_marker//;s/ /;/g" > /installed_dependencies

# Needed to send HTTP requests from the app test driver
pip3 install requests
