from subprocess import call
import copy
import json
import requests

from tests.utils import utils
import engine
from engine.evidence import write_evidence

def get_test_repo_host():
    """Returns the host name used by the locally served test git repository."""

    return "localhost:%d" % utils.REMOTE_PORT

def get_test_gitect_host():
    """Returns the host name used by the test gitect instance used by this test driver."""

    return "localhost:8000"


def get_evidence_endpoint(branch, commit, evidence_type):
    """Returns the evidence endpoint for the specified branch, commit and evidence type."""

    endpoint_template = "http://%s/api/v1/%s/%s/remote/~/%s/%s/evidence/%s"
    return endpoint_template % (get_test_gitect_host(),
                                get_test_repo_host(),
                                utils.TEST_DIR,
                                branch,
                                commit,
                                evidence_type)


def test_authentication(login, password):
    """Tests the authentication mechanism."""

    utils.reset_test_environment(login, password)
    commit = utils.get_last_commit_sha()

    evidence_endpoint = get_evidence_endpoint("master", commit, "change-validation")

    # Test case 1: existing host, wrong host token
    test = requests.get(evidence_endpoint,
                        headers={"Accept": "application/json", "X-Gitect-Token": "Wrong_token"})
    assert test.status_code == 401

    # Test case 2: non existent host, correct token
    test = requests.get(evidence_endpoint.replace(get_test_repo_host(), "gitlab42.biz"),
                        headers={"Accept": "application/json",
                                 "X-Gitect-Token": engine.config.get('TOKEN')})
    assert test.status_code == 500

    # Test case 3: existing host, correct token
    test = requests.get(evidence_endpoint,
                        headers={"Accept": "application/json",
                                 "X-Gitect-Token": engine.config.get('TOKEN')})
    assert test.status_code == 200


def test_evidence_endpoint(login, password):
    """Tests the evidence endpoint."""

    utils.reset_test_environment(login, password)
    commit = utils.get_last_commit_sha()
    evidence_type = "change-validation"
    evidence_endpoint = get_evidence_endpoint("master", commit, evidence_type)

    # Test case 1: wrong `Accept` header
    test = requests.get(evidence_endpoint,
                        headers={"Accept": "application/html",
                                 "X-Gitect-Token": engine.config.get('TOKEN')})
    assert test.status_code == 406

    # Test case 2: wrong evidence type
    test = requests.get(evidence_endpoint.replace(evidence_type, "nonexistent-type"),
                        headers={"Accept": "application/json",
                                 "X-Gitect-Token": engine.config.get('TOKEN')})
    assert test.status_code == 404

    # Test case 3: wrong token
    test = requests.get(evidence_endpoint,
                        headers={"Accept": "application/json",
                                 "X-Gitect-Token": "Wrong_token"})
    assert test.status_code == 401

    # Test case 4: wrong branch
    test = requests.get(get_evidence_endpoint("wrong-branch", commit, evidence_type),
                        headers={"Accept": "application/json",
                                 "X-Gitect-Token": engine.config.get('TOKEN')})
    assert test.status_code == 404

    # Test case 5: non existent evidence ID
    test = requests.get(evidence_endpoint + "/42",
                        headers={"Accept": "application/json",
                                 "X-Gitect-Token": engine.config.get('TOKEN')})
    assert test.status_code == 404

    # Test case 6: should succeed
    test = requests.get(evidence_endpoint,
                        headers={"Accept": "application/json",
                                 "X-Gitect-Token": engine.config.get('TOKEN')})
    assert test.status_code == 200
    assert test.json() == {'elements': []}

    # Test case 7: post with wrong Content-Type header
    evidence_data = utils.get_evidence_to_write()[evidence_type]
    test = requests.post(evidence_endpoint,
                         data=json.dumps(evidence_data),
                         headers={"Content-Type": "application/html",
                                  "X-Gitect-Token": engine.config.get('TOKEN')})
    assert test.status_code == 415

    # Test case 8: post with non-compliant evidence data
    invalid_data = copy.deepcopy(evidence_data)
    invalid_data['elements'][0]['schema-version'] = "" # made not schema-compliant
    test = requests.post(evidence_endpoint,
                         data=json.dumps(invalid_data),
                         headers={"Content-Type": "application/json",
                                  "X-Gitect-Token": engine.config.get('TOKEN')})
    assert test.status_code == 400

    # Test case 9: post with internal git server error (eg. due to non existent branch)
    evidence_data = utils.get_evidence_to_write()[evidence_type]
    test = requests.post(get_evidence_endpoint("wrong-branch", commit, evidence_type),
                         data=json.dumps(evidence_data),
                         headers={"Content-Type": "application/json",
                                  "X-Gitect-Token": engine.config.get('TOKEN')})
    assert test.status_code == 503

    # Test case 10: should succeed
    test = requests.post(evidence_endpoint,
                         data=json.dumps(evidence_data),
                         headers={"Content-Type": "application/json",
                                  "X-Gitect-Token": engine.config.get('TOKEN')})
    assert test.status_code == 200
    assert test.text == "OK"

    # Test case 11: should succeed
    test = requests.get(evidence_endpoint,
                        headers={"Accept": "application/json",
                                 "X-Gitect-Token": engine.config.get('TOKEN')})
    assert test.status_code == 200
    assert test.json() == evidence_data

    # Test case 12: non existent evidence ID
    test = requests.get(evidence_endpoint + "/wrong_evidence_id",
                        headers={"Accept": "application/json",
                                 "X-Gitect-Token": engine.config.get('TOKEN')})
    assert test.status_code == 404

    # Test case 13: should succeed
    test = requests.get(evidence_endpoint + "/change_validation_test_id",
                        headers={"Accept": "application/json",
                                 "X-Gitect-Token": engine.config.get('TOKEN')})
    assert test.status_code == 200
    assert test.json() == evidence_data["elements"][0]


def test_history_endpoint():
    """Tests the history endpoint."""

    endpoint_template = "http://%s/api/v1/%s/%s/remote/~/%s/%s/history"

    history_endpoint = endpoint_template % (get_test_gitect_host(),
                                            get_test_repo_host(),
                                            utils.TEST_DIR,
                                            "master",
                                            utils.get_last_commit_sha())

    # Test case 1: wrong `Accept` header
    test = requests.get(history_endpoint,
                        headers={"Accept": "application/html",
                                 "X-Gitect-Token": engine.config.get('TOKEN')})
    assert test.status_code == 406

    # Test case 2: wrong token
    test = requests.get(history_endpoint,
                        headers={"Accept": "application/json",
                                 "X-Gitect-Token": "Wrong_token"})
    assert test.status_code == 401

    # Test case 3: should succeed
    test = requests.get(history_endpoint,
                        headers={"Accept": "application/json",
                                 "X-Gitect-Token": engine.config.get('TOKEN')})
    assert test.status_code == 200


def test_merges_endpoint(login, password):
    """Tests the merges endpoint."""

    endpoint_template = "http://%s/api/v1/gitlab/%s/merges"
    merges_endpoint = endpoint_template % (get_test_gitect_host(), get_test_repo_host())

    # Test case 1: wrong `Content-Type` header
    test = requests.post(merges_endpoint,
                         data={},
                         headers={"Content-Type": "application/html",
                                  "X-Gitlab-Token": engine.config.get('TOKEN')})
    assert test.status_code == 415

    # Test case 2: wrong token
    test = requests.post(merges_endpoint,
                         data={},
                         headers={"Content-Type": "application/json",
                                  "X-Gitlab-Token": "Wrong_token"})
    assert test.status_code == 401

    # Test case 3: empty data
    test = requests.post(merges_endpoint,
                         data={},
                         headers={"Content-Type": "application/json",
                                  "X-Gitlab-Token": engine.config.get('TOKEN')})
    assert test.status_code == 400

    # Test case 4: wrong `action` field
    merge_data = {'object_attributes' : {'action' : 'not_merge'}}
    test = requests.post(merges_endpoint,
                         data=json.dumps(merge_data),
                         headers={"Content-Type": "application/json",
                                  "X-Gitlab-Token": engine.config.get('TOKEN')})
    assert test.status_code == 200
    assert test.text == "OK"

    merge_commit_sha = utils.get_last_commit_sha()
    merge_data = {'project' : {'path_with_namespace' : utils.TEST_DIR + '/remote'},
                  'object_attributes' : {
                      'action' : 'merge',
                      'source_branch' : 'test_branch_no_mr',
                      'target_branch' : 'master',
                      'merge_commit_sha' : merge_commit_sha}
                 }

    # Test case 5: source branch with no evidence
    test = requests.post(merges_endpoint,
                         data=json.dumps(merge_data),
                         headers={"Content-Type": "application/json",
                                  "X-Gitlab-Token": engine.config.get('TOKEN')})
    assert test.status_code == 500

    # Test case 6: should succeed
    test_branch = "test_branch_merged"
    merge_data["object_attributes"]["source_branch"] = test_branch
    utils.reset_test_environment(login, password)
    evidence_to_write = utils.get_evidence_to_write()
    commit = utils.create_test_commit(branch=test_branch)

    for evidence_type in evidence_to_write:
        write_evidence("localhost:%d" % utils.REMOTE_PORT,
                       "%s/remote" % utils.TEST_DIR,
                       login,
                       password,
                       test_branch,
                       commit,
                       evidence_type,
                       evidence_to_write[evidence_type]["elements"])

    call(['git', 'checkout', 'master'])
    call(['git', 'merge', test_branch])
    call(['git', 'push'])

    merge_commit_sha = utils.get_last_commit_sha()
    merge_data["object_attributes"]["merge_commit_sha"] = merge_commit_sha

    test = requests.post(merges_endpoint,
                         data=json.dumps(merge_data),
                         headers={"Content-Type": "application/json",
                                  "X-Gitlab-Token": engine.config.get('TOKEN')})
    assert test.status_code == 200
    assert test.text == "OK"


def test_policy_compliance_endpoint():
    """Tests the policy compliance endpoint."""

    endpoint_template = "http://%s/api/v1/%s/%s/~/%s/%s/policy-compliance"
    commit = utils.create_test_commit(branch="test_branch")
    policy_compliance_endpoint = endpoint_template % (get_test_gitect_host(),
                                                      get_test_repo_host(),
                                                      "%s/remote" % utils.TEST_DIR,
                                                      "test_branch",
                                                      commit)

    # Test case 1: wrong `Accept` header
    test = requests.get(policy_compliance_endpoint,
                        data={},
                        headers={"Accept": "application/html",
                                 "X-Gitect-Token": engine.config.get('TOKEN')})
    assert test.status_code == 406

    # Test case 2: wrong token
    test = requests.get(policy_compliance_endpoint,
                        data={},
                        headers={"Accept": "application/json",
                                 "X-Gitect-Token": "Wrong_token"})
    assert test.status_code == 401

    # Test case 3: should succeed (other policy compliance cases are covered by
    # the engine test driver)
    test = requests.get(policy_compliance_endpoint,
                        data={},
                        headers={"Accept": "application/json",
                                 "X-Gitect-Token": engine.config.get('TOKEN')})
    assert test.status_code == 200
    assert test.json() == {'result': True}
