import os
import shutil
import datetime

from subprocess import call, check_output

import engine

REMOTE_PORT = 4242
SERVER_ROOT = "/var/www/html/git"
TEST_DIR = "test_evidence_collection"
REMOTE_PATH = os.path.join(SERVER_ROOT, TEST_DIR)
CLONE_PATH = "/tmp/clone"

EVIDENCE_TO_WRITE = {}

def create_test_commit(branch="master", check_head=True):
    """Create a test commit on the specified branch, based on the current date
    and time. If the branch does not exist, it is created."""

    if check_head:
        current_branch = check_output(['git',
                                       'rev-parse',
                                       '--abbrev-ref',
                                       'HEAD']).strip().decode("utf-8")

        if current_branch != branch:
            if call(['git', 'checkout', branch]):
                # The branch does not exist, let's create it
                call(['git', 'checkout', '-b', branch])

    timestamp = datetime.datetime.utcnow().strftime("%Y%M%d%H%M%S%f")
    test_filename = "test_file_%s" % timestamp
    with open(test_filename, 'a') as test_file:
        test_file.write('This is another test file: %s\n' % timestamp)

    call(['git', 'add', test_filename])
    call(['git', 'commit', '-m', 'Test commit %s' % timestamp])
    call(['git', 'push', '-u', 'origin', branch])

    return get_last_commit_sha(branch)


def create_test_environment(login, password):
    """Creates an environment to use for testing, with a fresh clone and a
    remote on the same host.

    Note: this requires a web sever configured on the host to serve git
    repositories.
    """

    # Set up a local clone/remote pair
    remote_dir = os.path.join(REMOTE_PATH, "remote.git")
    os.makedirs(remote_dir)
    os.chdir(remote_dir)
    call(['git', 'init', '--bare'])
    call(['git', 'config', '--local', 'http.receivepack', 'true'])
    call(['git', 'update-server-info'])

    for root, dirs, files in os.walk(remote_dir):
        shutil.chown(root, "www-data", "www-data")
        for curr_dir in dirs:
            shutil.chown(os.path.join(root, curr_dir), "www-data", "www-data")
        for curr_file in files:
            shutil.chown(os.path.join(root, curr_file), "www-data", "www-data")

    clone_url = 'http://%s:%s@localhost:%d/%s/remote' % (login, password, REMOTE_PORT, TEST_DIR)
    call(['git', 'clone', clone_url, CLONE_PATH])
    os.chdir(CLONE_PATH)

    call(['git', 'config', '--local', 'user.name', 'Gitect Test CI'])
    call(['git', 'config', '--local', 'user.email', 'em@il.com'])

    # Create a first commit to initiate the remote
    create_test_commit("master", False)


def maybe_teardown_clone_pool():
    """Tears down the clone pool, if any.
    """

    clone_pool_dir = os.path.join(engine.config.get("DATA_DIR"),
                                  "localhost:%d" % REMOTE_PORT)
    if os.path.isdir(clone_pool_dir):
        shutil.rmtree(clone_pool_dir)


def delete_test_environment():
    """Destroys a test environment created with create_test_environment(),
    along with a possible clone pool.
    """

    if os.path.isdir(CLONE_PATH):
        shutil.rmtree(CLONE_PATH)

    if os.path.isdir(REMOTE_PATH):
        shutil.rmtree(REMOTE_PATH)

    maybe_teardown_clone_pool()


def reset_test_environment(login, password):
    """Deletes any existing test environment, then recreates a fresh one.
    """

    delete_test_environment()
    create_test_environment(login, password)


def get_last_commit_sha(branch="master"):
    """Returns the SHA of the last commit on the test repository.
    """

    return check_output(["git", "rev-parse", branch]).strip().decode("utf-8")


def get_evidence_to_write():
    """Returns a list of evidence to write. Useful for comparison of read and
    written evidence.
    """

    # pylint: disable=global-statement
    global EVIDENCE_TO_WRITE

    if EVIDENCE_TO_WRITE:
        # The evidence has already been fetched once; generating it again would
        # lead to datetime differences
        return EVIDENCE_TO_WRITE

    for evidence_type in engine.config.get("EVIDENCE_TYPES"):

        current_evidence = {}
        current_evidence["date-time"] = datetime.datetime.utcnow().isoformat()
        current_evidence["schema-version"] = "1"
        current_evidence["feature"] = "master"

        if evidence_type == "environment-construction":
            current_evidence["id"] = "environment_test_id"
            current_evidence["environment-type"] = "environment_test_type"
            current_evidence["orchestrator-id"] = "orchestrator_test_id"

        if evidence_type == "artefact-construction":
            current_evidence["id"] = "artefact_test_id"
            current_evidence["artefact-name"] = "artefact_test.name"
            current_evidence["contributor-id"] = "Charlie Tango"

        if evidence_type == "artefact-deployment":
            current_evidence["id"] = "artefact_deployment_test_id"
            current_evidence["artefact-id"] = "artefact_test_id"
            current_evidence["orchestrator-id"] = "orchestrator_test_id"
            current_evidence["environment-id"] = "environment_test_id"

        if evidence_type == "change-validation":
            current_evidence["id"] = "change_validation_test_id"
            current_evidence["validation-results-list"] = ["result%d" % num for num in range(1, 4)]
            current_evidence["change-validation-type"] = "change_validation_test_type"
            current_evidence["change-validator-id"] = "change_validator_test_id"
            current_evidence["environment-id"] = "environment_test_id"

        if evidence_type == "change-review":
            current_evidence["id"] = "change_review_test_id"
            current_evidence["change-submission-id-list"] = ["subm%d" % num for num in range(1, 6)]
            current_evidence["change-validation-id-list"] = ["valid%d" % num for num in range(1, 3)]
            current_evidence["reviewer-id-list"] = ["Foo Bar", "Charlie Tango"]

            patch_set_id = "test_patch_id"
            current_evidence["comments"] = []

            for idx in range(1, 5):
                current_comment = {}
                current_comment["key"] = {}
                current_comment["key"]["id"] = "comment%d_keyId" % idx
                current_comment["key"]["patch-set-id"] = patch_set_id

                current_comment["author"] = {}
                current_comment["author"]["id"] = "author_id"

                current_comment["written-on"] = datetime.datetime.utcnow().isoformat()
                current_comment["rev-id"] = "rev_test_id"
                current_comment["unresolved"] = idx % 2 == 0
                current_comment["message"] = "Hello, this is message %d." % idx

                current_evidence["comments"].append(current_comment)
        else:
            current_evidence["comment"] = "This is a test comment for %s evidence." \
            % evidence_type.replace("-", " ")

        evidence_dict = {}
        evidence_dict["elements"] = [current_evidence]

        EVIDENCE_TO_WRITE[evidence_type] = evidence_dict

    return EVIDENCE_TO_WRITE
