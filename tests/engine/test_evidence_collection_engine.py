from subprocess import call, check_output

import pytest
from tests.utils import utils
import engine
from engine.evidence import read_evidence, write_evidence
from engine.policy import merge, check_policy_compliance
from engine.history import get_history
from engine.errors import PolicyError, ValidationError, GitServerError


def update_policy_config(approval_text, approval_count, unique_approvers_count):
    """Updates the policy configuration with the specified text, expected
    approvals count and unique approvers count."""

    pattern = {}
    pattern["text"] = approval_text
    pattern["count"] = approval_count
    approvals_policy = {}
    approvals_policy["pattern-list"] = [pattern]

    if unique_approvers_count:
        approvals_policy["unique-approvers"] = unique_approvers_count

    default_policy = {}
    default_policy["approval-policy"] = approvals_policy
    localhost_policy = {}
    localhost_policy["default"] = default_policy
    custom_policy_config = {}
    custom_policy_config["localhost:%d" % utils.REMOTE_PORT] = localhost_policy

    engine.config.update(POLICY=custom_policy_config)


def test_write_evidence(login, password):
    """Tests evidence writing functionality.
    """

    utils.reset_test_environment(login, password)
    evidence_to_write = utils.get_evidence_to_write()

    commit = utils.get_last_commit_sha()

    for evidence_type in evidence_to_write:
        write_evidence("localhost:%d" % utils.REMOTE_PORT,
                       "%s/remote" % utils.TEST_DIR,
                       login,
                       password,
                       "master",
                       commit,
                       evidence_type,
                       evidence_to_write[evidence_type]["elements"])

    # Test that a wrong parameter results in a failure.
    # Expectedly failing attempts are made against a separate branch, to
    # prevent conflicts with `test_read_evidence`

    failures_branch = "test-failed-writes"
    call(['git', 'checkout', '-b', failures_branch])

    # Create a commit to make sure write attempts are made on a different SHA
    # from that of `master`
    commit = utils.create_test_commit(failures_branch)

    # Wrong host
    utils.maybe_teardown_clone_pool()
    with pytest.raises(GitServerError):
        write_evidence("wrong.host:%d" % utils.REMOTE_PORT,
                       "%s/remote" % utils.TEST_DIR,
                       login,
                       password,
                       failures_branch,
                       commit,
                       "environment-construction",
                       evidence_to_write["environment-construction"]["elements"])

    # Wrong project
    utils.maybe_teardown_clone_pool()
    with pytest.raises(GitServerError):
        write_evidence("localhost:%d" % utils.REMOTE_PORT,
                       "%s/wrong_project" % utils.TEST_DIR,
                       login,
                       password,
                       failures_branch,
                       commit,
                       "environment-construction",
                       evidence_to_write["environment-construction"]["elements"])

    # Wrong login
    utils.maybe_teardown_clone_pool()
    with pytest.raises(GitServerError):
        write_evidence("localhost:%d" % utils.REMOTE_PORT,
                       "%s/remote" % utils.TEST_DIR,
                       "wrong_login",
                       password,
                       failures_branch,
                       commit,
                       "environment-construction",
                       evidence_to_write["environment-construction"]["elements"])

    # Wrong password
    utils.maybe_teardown_clone_pool()
    with pytest.raises(GitServerError):
        write_evidence("localhost:%d" % utils.REMOTE_PORT,
                       "%s/remote" % utils.TEST_DIR,
                       login,
                       "wrong_password",
                       failures_branch,
                       commit,
                       "environment-construction",
                       evidence_to_write["environment-construction"]["elements"])

    # Wrong branch
    utils.maybe_teardown_clone_pool()
    with pytest.raises(GitServerError):
        write_evidence("localhost:%d" % utils.REMOTE_PORT,
                       "%s/remote" % utils.TEST_DIR,
                       login,
                       password,
                       "wrong_branch",
                       commit,
                       "environment-construction",
                       evidence_to_write["environment-construction"]["elements"])

    # Wrong commit
    utils.maybe_teardown_clone_pool()
    with pytest.raises(GitServerError):
        write_evidence("localhost:%d" % utils.REMOTE_PORT,
                       "%s/remote" % utils.TEST_DIR,
                       login,
                       password,
                       failures_branch,
                       "0000000000",
                       "environment-construction",
                       evidence_to_write["environment-construction"]["elements"])

    # No evidence type
    utils.maybe_teardown_clone_pool()
    with pytest.raises(ValidationError):
        write_evidence("localhost:%d" % utils.REMOTE_PORT,
                       "%s/remote" % utils.TEST_DIR,
                       login,
                       password,
                       failures_branch,
                       commit,
                       None,
                       evidence_to_write["environment-construction"]["elements"])

    # No evidence
    utils.maybe_teardown_clone_pool()
    with pytest.raises(ValidationError):
        write_evidence("localhost:%d" % utils.REMOTE_PORT,
                       "%s/remote" % utils.TEST_DIR,
                       login,
                       password,
                       failures_branch,
                       commit,
                       "environment-construction",
                       None)


def test_read_evidence(login, password):
    """Tests evidence reading functionality.
    """

    evidence_to_write = utils.get_evidence_to_write()

    commit = utils.get_last_commit_sha()

    # First test that a wrong parameter results in a failure
    # Wrong host
    utils.maybe_teardown_clone_pool()
    with pytest.raises(GitServerError):
        evidence_list = read_evidence("wrong.host:%d" % utils.REMOTE_PORT,
                                      "%s/remote" % utils.TEST_DIR,
                                      login,
                                      password,
                                      "master",
                                      commit,
                                      ["environment-construction"])

    # Wrong project
    utils.maybe_teardown_clone_pool()
    with pytest.raises(GitServerError):
        evidence_list = read_evidence("localhost:%d" % utils.REMOTE_PORT,
                                      "%s/wrong_project" % utils.TEST_DIR,
                                      login,
                                      password,
                                      "master",
                                      commit,
                                      ["environment-construction"])

    # Wrong login
    utils.maybe_teardown_clone_pool()
    with pytest.raises(GitServerError):
        evidence_list = read_evidence("localhost:%d" % utils.REMOTE_PORT,
                                      "%s/remote" % utils.TEST_DIR,
                                      "wrong_login",
                                      password,
                                      "master",
                                      commit,
                                      ["environment-construction"])

    # Wrong password
    utils.maybe_teardown_clone_pool()
    with pytest.raises(GitServerError):
        evidence_list = read_evidence("localhost:%d" % utils.REMOTE_PORT,
                                      "%s/remote" % utils.TEST_DIR,
                                      login,
                                      "wrong_password",
                                      "master",
                                      commit,
                                      ["environment-construction"])

    # Wrong branch
    utils.maybe_teardown_clone_pool()
    with pytest.raises(GitServerError):
        evidence_list = read_evidence("localhost:%d" % utils.REMOTE_PORT,
                                      "%s/remote" % utils.TEST_DIR,
                                      login,
                                      password,
                                      "wrong_branch",
                                      commit,
                                      ["environment-construction"])

    # Wrong commit
    utils.maybe_teardown_clone_pool()
    evidence_list = read_evidence("localhost:%d" % utils.REMOTE_PORT,
                                  "%s/remote" % utils.TEST_DIR,
                                  login,
                                  password,
                                  "master",
                                  "0000000000",
                                  ["environment-construction"])
    assert not evidence_list

    # No evidence type
    utils.maybe_teardown_clone_pool()
    evidence_list = read_evidence("localhost:%d" % utils.REMOTE_PORT,
                                  "%s/remote" % utils.TEST_DIR,
                                  login,
                                  password,
                                  "master",
                                  commit,
                                  [])
    assert not evidence_list

    for evidence_type in evidence_to_write:
        evidence_list = read_evidence("localhost:%d" % utils.REMOTE_PORT,
                                      "%s/remote" % utils.TEST_DIR,
                                      login,
                                      password,
                                      "master",
                                      commit,
                                      [evidence_type])

        assert evidence_list == evidence_to_write[evidence_type]["elements"]


def test_merge(login, password):
    """Tests merging functionality."""

    test_branch = "test_branch_merged"

    # Test case 1: No evidence written
    utils.reset_test_environment(login, password)
    utils.create_test_commit(branch=test_branch)

    assert not merge("localhost:%d" % utils.REMOTE_PORT,
                     "%s/remote" % utils.TEST_DIR,
                     login,
                     password,
                     test_branch,
                     "master",
                     utils.get_last_commit_sha())


    # Test case 2: with written evidence, merged request status merged
    check_merge(login, password, "test_branch_merged")

    # Test case 3: with written evidence, merged request status locked
    check_merge(login, password, "test_branch_locked")

def check_merge(login, password, test_branch):
    """Tests merge functionality."""

    utils.reset_test_environment(login, password)
    evidence_to_write = utils.get_evidence_to_write()
    commit = utils.create_test_commit(branch=test_branch)
    print(commit)

    for evidence_type in evidence_to_write:
        write_evidence("localhost:%d" % utils.REMOTE_PORT,
                       "%s/remote" % utils.TEST_DIR,
                       login,
                       password,
                       test_branch,
                       commit,
                       evidence_type,
                       evidence_to_write[evidence_type]["elements"])

    call(['git', 'checkout', 'master'])
    call(['git', 'merge', '--no-ff', test_branch])

    merge_commit_sha = utils.get_last_commit_sha()
    merge("localhost:%d" % utils.REMOTE_PORT,
          "%s/remote" % utils.TEST_DIR,
          login,
          password,
          test_branch,
          "master",
          merge_commit_sha)

    # Check that all evidence has been copied onto the merge commit
    for evidence_type in evidence_to_write:
        evidence_list = read_evidence("localhost:%d" % utils.REMOTE_PORT,
                                      "%s/remote" % utils.TEST_DIR,
                                      login,
                                      password,
                                      "master",
                                      merge_commit_sha,
                                      [evidence_type])
        # mock gitlab API does not provide correct matching data for
        # approvers/comments in change-review evidence
        if not evidence_type == "change-review":
            assert evidence_list == evidence_to_write[evidence_type]["elements"]


def test_check_policy_no_policy(login, password):
    """Tests policy compliance checks with no policy in place."""

    # Test case 1: check policy on a branch against which no merge request has
    # been submitted
    check_policy_no_policy(login, password, False)

    # Test case 2: check policy on a branch with a merge request
    check_policy_no_policy(login, password, True)


def check_policy_no_policy(login, password, with_mr):
    """Tests policy compliance with no policy in place. If `with_mr`
    evaluates to False, tests are run against a branch which does not have any
    associated merge request."""

    test_branch = "test_branch" if with_mr else "test_branch_no_mr"

    utils.reset_test_environment(login, password)
    commit = utils.create_test_commit(branch=test_branch)

    # Check that no change review evidence initially exists
    evidence_list = read_evidence("localhost:%d" % utils.REMOTE_PORT,
                                  "%s/remote" % utils.TEST_DIR,
                                  login,
                                  password,
                                  test_branch,
                                  commit,
                                  ["change-review"])
    assert not evidence_list

    if with_mr:
        check_policy_compliance("localhost:%d" % utils.REMOTE_PORT,
                                "%s/remote" % utils.TEST_DIR,
                                login,
                                password,
                                test_branch,
                                commit,
                                "master",
                                ["test_validation_id1", "test_validation_id2"])
    else:
        with pytest.raises(PolicyError):
            check_policy_compliance("localhost:%d" % utils.REMOTE_PORT,
                                    "%s/remote" % utils.TEST_DIR,
                                    login,
                                    password,
                                    test_branch,
                                    commit,
                                    "master",
                                    ["test_validation_id1", "test_validation_id2"])


    if not with_mr:
        # change review evidence is not copied if no merge request is
        # available, hence further checks are not relevant
        return

    # Check that change review evidence has been written
    evidence_list = read_evidence("localhost:%d" % utils.REMOTE_PORT,
                                  "%s/remote" % utils.TEST_DIR,
                                  login,
                                  password,
                                  test_branch,
                                  commit,
                                  ["change-review"])
    assert evidence_list


def test_check_policy_simple_policy(login, password):
    """Tests policy compliance checks with a simple approvals policy in place."""

    update_policy_config("APPROVED", 2, 2)

    # Test case 1: unique commenters, but no approval text in the notes
    test_branch = "test_branch"

    utils.reset_test_environment(login, password)
    commit = utils.create_test_commit(branch=test_branch)

    with pytest.raises(PolicyError):
        check_policy_compliance("localhost:%d" % utils.REMOTE_PORT,
                                "%s/remote" % utils.TEST_DIR,
                                login,
                                password,
                                test_branch,
                                commit,
                                "master",
                                ["test_validation_id1", "test_validation_id2"])

    # Check that change review evidence has still been written
    evidence_list = read_evidence("localhost:%d" % utils.REMOTE_PORT,
                                  "%s/remote" % utils.TEST_DIR,
                                  login,
                                  password,
                                  test_branch,
                                  commit,
                                  ["change-review"])
    assert evidence_list

    # Test case 2: non unique commenters, with approval text in the notes
    test_branch = "test_branch_with_approvals_no_unique_approver"

    utils.reset_test_environment(login, password)
    commit = utils.create_test_commit(test_branch)

    with pytest.raises(PolicyError):
        check_policy_compliance("localhost:%d" % utils.REMOTE_PORT,
                                "%s/remote" % utils.TEST_DIR,
                                login,
                                password,
                                test_branch,
                                commit,
                                "master",
                                ["test_validation_id1", "test_validation_id2"])

    # Check that change review evidence has been written anyway
    evidence_list = read_evidence("localhost:%d" % utils.REMOTE_PORT,
                                  "%s/remote" % utils.TEST_DIR,
                                  login,
                                  password,
                                  test_branch,
                                  commit,
                                  ["change-review"])
    assert evidence_list

    # Test case 3: unique commenters, with approval text in the notes
    test_branch = "test_branch_with_approvals"

    utils.reset_test_environment(login, password)
    commit = utils.create_test_commit(test_branch)

    check_policy_compliance("localhost:%d" % utils.REMOTE_PORT,
                            "%s/remote" % utils.TEST_DIR,
                            login,
                            password,
                            test_branch,
                            commit,
                            "master",
                            ["test_validation_id1", "test_validation_id2"])

    # Check that change review evidence has been written
    evidence_list = read_evidence("localhost:%d" % utils.REMOTE_PORT,
                                  "%s/remote" % utils.TEST_DIR,
                                  login,
                                  password,
                                  test_branch,
                                  commit,
                                  ["change-review"])
    assert evidence_list


def test_read_history(login, password):
    """Tests history reads."""

    check_read_history(login, password)


def check_read_history(login, password):
    """Test history reads."""

    utils.reset_test_environment(login, password)
    evidence_to_write = utils.get_evidence_to_write()

    test_branches = ["test_branch1", "test_branch2", "test_branch3"]
    written_evidence = {}
    merge_commits = {}

    # the evidence type does not matter here, and is picked arbitrarily
    evidence_type = "change-validation"

    for branch in test_branches:
        commit = utils.create_test_commit(branch)

        current_evidence = evidence_to_write[evidence_type]["elements"][0].copy()
        current_evidence["feature"] = branch
        write_evidence("localhost:%d" % utils.REMOTE_PORT,
                       "%s/remote" % utils.TEST_DIR,
                       login,
                       password,
                       branch,
                       commit,
                       evidence_type,
                       [current_evidence])

        written_evidence[branch] = [current_evidence]

        call(['git', 'checkout', 'master'])
        call(['git', 'merge', '--no-ff', branch])

        # a push needs to be done for each branch so that `merge()` (or more
        # specifically the mock Gitlab instance) has access to the merge commit
        call(['git', 'push'])

        merge_commits[branch] = check_output(['git', 'rev-parse', 'HEAD']).strip().decode("utf-8")

        try:
            merge("localhost:%d" % utils.REMOTE_PORT,
                  "%s/remote" % utils.TEST_DIR,
                  login,
                  password,
                  branch,
                  "master",
                  utils.get_last_commit_sha())
        except (PolicyError, ValidationError):
            pass

    commit = check_output(['git', 'rev-parse', 'HEAD']).strip().decode("utf-8")
    history_list = get_history("localhost:%d" % utils.REMOTE_PORT,
                               "%s/remote" % utils.TEST_DIR,
                               login,
                               password,
                               "master",
                               commit)

    for branch in test_branches:
        for history in history_list:
            if history["commit"] == merge_commits[branch]:
                matched_history = history
                break
        assert matched_history["evidence"] == written_evidence[branch]
