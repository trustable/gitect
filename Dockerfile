FROM python:latest
COPY . /gitect
WORKDIR /gitect
RUN pip install -r requirements.txt
ENV FLASK_APP=gitect.py
ENTRYPOINT ["flask"]
CMD ["run"]
