# Git Evidence Collection Tool

Git Evidence Collection Tool (**gitect**) is a proof-of-concept tool to enable
establishment of a trustable workflow for development of software as described
in <https://gitlab.com/trustable/documents/tree/master/deprecated>.

**gitect** is designed to perform the roles of Evidence Tracker, Policy Handler
and Resource Tracker as defined in the trustable workflow
[Roles](https://gitlab.com/trustable/documents/blob/master/deprecated/roles.md).

**gitect** allows the software development process to be instrumented to collect
evidence throughout the development lifecycle. It provides a frictionless method
to record the data evidence required to generate key metrics relating to the
software development process.

**gitect** currently supports projects developed using GitLab but could easily
be extended to support other Git servers.

For an overview of the software architecture employed by **gitect** refer to the
[Software Architecture](architecture.md).

To install **gitect** refer to the [Installation Guide](installation.md).

To configure the **gitect** installation refer to the
[Configuration Guide](configuration.md).

To setup a project to use **gitect** refer to the
[Usage Guide](usage.md).
