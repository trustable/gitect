import json
from flask import abort, request, Response
from app.app_def import app
from app.utils import authenticate, check_accept_header
from engine.policy import check_policy_compliance
from engine.errors import GitServerError, PolicyError, ValidationError

@app.route('/api/v1/<string:host>/<path:project>/~/<path:branch>/<string:commit>/policy-compliance',
           methods=['GET'])
def policy_compliance(host, project, branch, commit):
    """Endpoint to check compliance of branch with policy."""

    target_branch = request.args.get('target-branch', default='master')
    validation_id_list = request.args.getlist('validation-id')

    check_accept_header(request)
    username, password = authenticate(request.headers.get('X-Gitect-Token'), host)

    try:
        check_policy_compliance(host,
                                project,
                                username,
                                password,
                                branch,
                                commit,
                                target_branch,
                                validation_id_list)
        return Response(response=json.dumps({'result': True}),
                        status=200,
                        mimetype='application/json')
    except (GitServerError, ValidationError) as ex:
        abort(500, ex.message)
    except PolicyError as ex:
        return Response(response=json.dumps({'result': False,
                                             'message': ex.message}),
                        status=200,
                        mimetype='application/json')
