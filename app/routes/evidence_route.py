import json
from flask import abort, request, Response
from app.app_def import app
from app.utils import authenticate
from engine.evidence import read_evidence, write_evidence
from engine.validate import validate_evidence_collection, validate_evidence_type
from engine.errors import ValidationError, GitServerError


# pylint: disable=line-too-long
@app.route('/api/v1/<string:host>/<path:project>/~/<path:branch>/<string:commit>/evidence/<string:evidence_type>',
           methods=['GET', 'POST'])
@app.route('/api/v1/<string:host>/<path:project>/~/<path:branch>/<string:commit>/evidence/<string:evidence_type>/<string:evidence_id>',
           methods=['GET'])
def evidence(host, project, branch, commit, evidence_type, evidence_id=None):
    """Endpoint to store and retrieve evidence."""

    try:
        validate_evidence_type(evidence_type)
    except ValidationError as ex:
        abort(404, ex.message)

    username, password = authenticate(request.headers.get('X-Gitect-Token'), host)

    if not (username and password):
        abort(401)

    if request.method == 'POST':
        content = request.get_json()

        if not request.headers.get('Content-Type') == 'application/json':
            abort(415)

        try:
            validate_evidence_collection(evidence_type, content)
        except ValidationError as ex:
            abort(400, ex.message)

        try:
            write_evidence(host, project, username, password, branch, commit,
                           evidence_type, content['elements'], True)
        except GitServerError as ex:
            abort(503, ex.message)

        return Response('OK', status=200)

    # GET request
    if not request.headers.get('Accept') == 'application/json':
        abort(406)

    try:
        evidence_list = read_evidence(host, project, username, password, branch,
                                      commit, [evidence_type])
    except GitServerError as ex:
        abort(404, ex.message)

    if evidence_id:
        # form id field for evidence type
        selected_evidence = None
        for evidence_item in evidence_list:
            if evidence_item['id'] == evidence_id:
                selected_evidence = evidence_item
                break
        if not selected_evidence:
            abort(404)

        return Response(response=json.dumps(selected_evidence), status=200,
                        mimetype='application/json')

    return Response(response=json.dumps({'elements': evidence_list}), status=200,
                    mimetype='application/json')
