import json
from flask import abort, request, Response
from app.app_def import app
from app.utils import authenticate, check_accept_header
from engine.history import get_history
from engine.errors import GitServerError

@app.route('/api/v1/<string:host>/<path:project>/~/<path:branch>/<string:commit>/history',
           methods=['GET'])
def history(host, project, branch, commit):
    """Endpoint to get history of branch up to commit."""

    check_accept_header(request)
    username, password = authenticate(request.headers.get('X-Gitect-Token'), host)

    try:
        history_list = get_history(host, project, username, password, branch, commit)
    except GitServerError as ex:
        abort(503, ex.message)

    return Response(response=json.dumps({'elements': history_list}), status=200,
                    mimetype='application/json')
