from flask import abort, request, Response
from app.app_def import app
from app.utils import authenticate
from engine.policy import merge
from engine.errors import PolicyError, ValidationError, GitServerError


@app.route('/api/v1/gitlab/<string:host>/merges', methods=['POST'])
def merges(host):
    """Endpoint to handle merge request events from GitLab server."""

    if not request.headers.get('Content-Type') == 'application/json':
        abort(415)

    username, password = authenticate(request.headers.get('X-Gitlab-Token'), host)

    content = request.get_json()

    if content['object_attributes']['action'] == 'merge':

        project = content['project']['path_with_namespace']
        branch = content['object_attributes']['source_branch']
        target_branch = content['object_attributes']['target_branch']
        merge_commit_sha = content['object_attributes']['merge_commit_sha']

        try:
            merge(host, project, username, password, branch, target_branch, merge_commit_sha)
        except (PolicyError, ValidationError, GitServerError) as ex:
            return Response(ex.message, status=500)

    return Response('OK', status=200)
