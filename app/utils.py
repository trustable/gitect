from flask import abort
import engine

def check_accept_header(request):
    """Checks that accept header is application/json.

    Checks that the 'Accept' header of the specified request specifies
    'application/json', throws a 406 error if it does not.

    Args:
        request: request object

    """
    if not request.headers.get('Accept') == 'application/json':
        abort(406)


def authenticate(token, host):
    """Authenticates and returns host credentials.

    Args:
        host (str): FQDN of Git host
        token (str): Supplied gitect token

    Returns:
        username of service account
        password of service account

    """

    if token == engine.config.get('TOKEN'):
        username = engine.config.get('HOSTS')[host]['username']
        password = engine.config.get('HOSTS')[host]['token']
        return username, password

    # Wrong token
    abort(401)

    return None, None
