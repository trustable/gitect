import json
import sys
from flask import Flask, request
import engine
from app.app_def import app

from app.routes import evidence_route
from app.routes import policy_compliance_route
from app.routes import merges_route
from app.routes import history_route

@app.before_request
def log_request_info():
    # mask tokens in logs
    engine.logger.info('Headers: %s',
                       {k:('********' if 'Token' in k else v)
                        for (k, v) in request.headers.items()})
    engine.logger.info('Body: %s', request.get_data())
