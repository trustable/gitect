import os
import logging
from flask import Flask
from flask.logging import default_handler

import engine

# pylint: disable=invalid-name
app = Flask(__name__)

engine.logger.addHandler(default_handler)

if os.environ.get('FLASK_ENV', None) != "development":
    gunicorn_logger = logging.getLogger('gunicorn.error')
    engine.logger.handlers = gunicorn_logger.handlers
    engine.logger.setLevel(gunicorn_logger.level)
